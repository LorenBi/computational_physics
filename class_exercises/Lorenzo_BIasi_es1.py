from numpy import zeros
import matplotlib.pyplot as plt
import numpy as np

# The function we want to interpolate


def f(x):
    return 1.0 / (1 + x ** 2)


# The algorithm for aquiring the newton coeff
def Newtoncoefficient(fi, xi):
    n = len(fi)
    coeff = initializecoefficient(fi, n)
    for j in range(1, n):
        for i in range(0, n - j):
            coeff[i][j] = (coeff[i + 1, j - 1] - coeff[i, j - 1]
                           ) / (xi[i + j] - xi[i])
    return coeff


def initializecoefficient(fi, n):
    coeff = zeros([n, n])
    for i in range(n):
        for j in range(n):
            if j == 0:
                coeff[i][j] = fi[i]
    return coeff


# The result of the interpolation in x. ATTENTION it returns the value of the
# interpolation in x and not a symbolic function
def poli_Newton(fi, xi, x):
    coeff = Newtoncoefficient(fi, xi)
    poli = coeff[0, 0]
    product = 1
    for i in range(1, n):
        product = product * (x - xi[i - 1])
        poli += coeff[0][i] * product
    return poli


def equidistant_array(start, finish, how_many):
    dist = [start]
    step = float(finish - start) / (how_many - 1)
    x = start
    for i in range(how_many - 1):
        x += step
        dist.append(x)
    return dist


# The main function are written above, from now on is just testing how it works

# FIRST WITH 6 POINTS
xi = equidistant_array(-5, 5, 6)
fi = [f(point) for point in xi]
n = len(xi)

print 'The coeffient:', Newtoncoefficient(fi, xi)[0]

test_x = [-4.92, -2.67, -1.58, 0.88, 2.22, 3.14, 4.37]

print 'Testing the interpolation in various points:\n'
for x in test_x:
    print 'x = ' + str(x)
    print 'The function: ' + str(f(x)) + ' the interpoled one: ' + str(poli_Newton(fi, xi, x))

fist_inter = np.array([poli_Newton(fi, xi, point)
                       for point in equidistant_array(-5, 5, 10)])

xi = equidistant_array(-5, 5, 20)
fi = [f(point) for point in xi]
n = len(xi)

# SECOND WITH 10 POINTS
test2_x = [-4.75, 4.8]
print '\nTesting the new interpolation'
for x in test2_x:
    print 'x = ' + str(x)
    print 'The function: ' + str(f(x)) + ' the interpoled one: ' + str(poli_Newton(fi, xi, x))


# THE PLOT
plt.figure('Interpole test')
plt.grid(True)
plt.plot(xi, np.array([f(point) for point in xi]), '.')
plt.plot(equidistant_array(-5, 5, 40),
         np.array([poli_Newton(fi, xi, point) for point in equidistant_array(-5, 5, 40)]))
plt.plot(equidistant_array(-5, 5, 10), fist_inter)
plt.show()

xi = equidistant_array(-5, 5, 6)
fi = [f(point) for point in xi]

print Newtoncoefficient(fi, xi)

# We can see that with 20 points we have the Runge's phenomenon, if we have
# so many point we should interpole them using a spline for avoinding the
# 'noise' an the ends
