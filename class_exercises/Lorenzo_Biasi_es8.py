import numpy as np
from random import random, uniform
import matplotlib.pyplot as plt


def simulate_decay(n_atoms, probability):
    iteration = 0
    nuclei = np.ones(n_atoms)
    n_active = n_atoms
    while n_active > 0:
        for i in range(n_atoms):
            if random() < probability and nuclei[i] != 0:
                nuclei[i] = 0
                n_active -= 1
        iteration += 1
    return iteration


def simultaneous_walks(step, n_drunks):
    positions = np.zeros((n_drunks, 2))
    N_ITER_MAX = 100
    mean_x = np.zeros(N_ITER_MAX)
    mean_y = np.zeros(N_ITER_MAX)
    mean_x2 = np.zeros(N_ITER_MAX)
    mean_y2 = np.zeros(N_ITER_MAX)
    for iteration in range(N_ITER_MAX):
        for i in range(n_drunks):
            angle = uniform(0, 2 * np.pi)
            positions[i, :] += np.array([np.cos(angle), np.sin(angle)]) * step
            mean_x[iteration] += positions[i, 0]
            mean_y[iteration] += positions[i, 1]
            mean_x2[iteration] += positions[i, 0] ** 2
            mean_y2[iteration] += positions[i, 1] ** 2
        mean_x[iteration] = mean_x[iteration] / float(n_drunks)
        mean_y[iteration] = mean_y[iteration] / float(n_drunks)
        mean_x2[iteration] = mean_x2[iteration] / float(n_drunks)
        mean_y2[iteration] = mean_y2[iteration] / float(n_drunks)

    return mean_x, mean_y, mean_x2, mean_y2


def montecarlo_quart_circle(n_shoot):
    n_hits = 0
    for i in range(n_shoot):
        if random() < np.sqrt(1 - random() ** 2):
            n_hits += 1
    return n_hits


def problem1():
    n_atoms = 1000
    probability = .2
    N_TIMES = 20
    average = 0
    for i in range(N_TIMES):
        average += simulate_decay(n_atoms, probability)
    print 'Average life time for'
    print 'p =', probability, ' and number of atoms', n_atoms
    print average / float(N_TIMES)


def problem2():
    mean_x, mean_y, mean_x2, mean_y2 = simultaneous_walks(1, 1000)
    iterations = np.arange(1, len(mean_x) + 1)

    plt.figure('random_walk')
    plt.rc('text', usetex=True)
    plt.plot(iterations, mean_x, '.', label=r'$<x>$')
    plt.plot(iterations, mean_x2, '.', label=r'$<x^2>$')
    plt.plot(iterations, mean_y, '.', label=r'$<y>$')
    plt.plot(iterations, mean_y2, '.', label=r'$<y^2>$')
    plt.plot(iterations, iterations / 2., label=r'$\frac{t}{2}$')
    plt.legend()
    plt.grid()


def problem3():
    n_total = 100000
    print 'Montecarlo value:'
    print montecarlo_quart_circle(n_total) / float(n_total)
    print 'Actual value:'
    print np.pi / 4.


if __name__ == '__main__':
    print 'Problem 1 \n'
    problem1()
    problem2()
    print '\n\nProblem 2 \n'
    problem3()
    plt.show()

