import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pylab as pl


# The next 4 functions are used to rapresent the differential equation
# You need to always insert the variable t (float) and the a second variable
# at time t, that can be an array (dimensions (n,)) if you work with a system
# of equations


# The next 3 function rapresent a falling object with a different coefficient
# k for friction
def falling_diff(t, p, k=0):
    M = 75.
    g = 9.8
    return - M * g + k * (p / M) ** 2


def falling_diff_k1(t, p):
    return falling_diff(t, p, k=0.3096)


def falling_diff_k2(t, p):
    return falling_diff(t, p, k=30.96)


# This is the diff equation for the 3D Lorenz attractor
def Lorenz_attractor(t, x):
    sigma = 10
    b = float(8) / float(3)
    r = 28
    dx1 = - sigma * x[0] + sigma * x[1]
    dx2 = - x[0] * x[2] + r * x[0] - x[1]
    dx3 = x[0] * x[1] - b * x[2]
    dx = np.array([dx1, dx2, dx3])
    return dx


def Lane_Emden(csi, x, exp):
    if x[0] == 0:
        dphi = -x[1] ** exp
    else:
        dphi = - (2 * x[0]) / csi - x[1] ** exp
    dtheta = x[0]
    return np.array([dphi, dtheta], dtype=complex)


# This methods solve differential equation
# interval is a tuple with length 2 and it rapresents the ends of the
# indipendent variable (in our case t)
# N is the number of sub intervals used
# diff_fun is the differnetial function used and defined in the same method
# as the previous
# the initial_cond is a tuple that contains the initial conditions for all the
# variables, even the independent one.
def Rounge_Kutta_4(interval, N, diff_fun, initial_cond, dtype=float):
    h = abs(interval[1] - interval[0]) / float(N)
    t = np.zeros(N)
    x = np.zeros((len(initial_cond) - 1, N), dtype=dtype)
    initialize(t, x, initial_cond)
    for n in range(N - 1):
        # No need of running a cycle on the number of variables since we use
        # an ndarray format.
        k_1 = diff_fun(t[n], x[:, n])
        k_2 = diff_fun(t[n] + h / 2, x[:, n] + (h * k_1) / 2)
        k_3 = diff_fun(t[n] + h / 2, x[:, n] + (h * k_2) / 2)
        k_4 = diff_fun(t[n] + h, x[:, n] + h * k_3)
        x[:, n + 1] = x[:, n] + (h * (k_1 + 2 * k_2 + 2 * k_3 + k_4)) / 6
        t[n + 1] = t[n] + h
    return t, x


# It gives the initial conditions
def initialize(t, x, initial_cond):
    t[0] = initial_cond[0]
    for i in range(1, len(initial_cond)):
        x[i - 1][0] = initial_cond[i]


def Predictor_Corrector(interval, N, diff_fun, initial_cond):
    h = abs(interval[1] - interval[0]) / float(N)
    t = np.zeros(N)
    x = np.zeros((len(initial_cond) - 1, N))
    initialize(t, x, initial_cond)
    t[0:4], x[:, 0:4] = Rounge_Kutta_4((0, 4 * h), 4, diff_fun, initial_cond)
    for n in range(3, N - 1):
        temp_x = x[:, n] + (h / 24.) * (55. * diff_fun(t[n], x[:, n]) - 59. * diff_fun(t[n - 1], x[
            :, n - 1]) + 37 * diff_fun(t[n - 2], x[:, n - 2]) - 9 * diff_fun(t[n - 3], x[:, n - 3]))
        t[n + 1] = t[n] + h
        x[:, n + 1] = x[:, n] + (h / 24.) * (9 * diff_fun(t[n + 1], temp_x) + 19 * diff_fun(
            t[n], x[:, n]) - 5 * diff_fun(t[n - 1], x[:, n - 1]) + diff_fun(t[n - 2], x[:, n - 2]))
    return t, x


# OUTPUT FUNCTIONS


def problem_1():
    t, p = Predictor_Corrector((0, 20), 5000, falling_diff, (0, 0))
    print 'The velocity calculate with k = 0 and the analitic solution:'
    print '%.1f' % (p[-1][-1] / 75), -9.8 * 20, '\n'
    print 'The velocity calculate with k = 0.3096'
    t, p = Predictor_Corrector((0, 20), 5000, falling_diff_k1, (0, 0))
    print '%.1f' % (p[-1][-1] / 75)
    print 'Ouch!!! \n'
    print 'The velocity calculate with k = 30.96'
    t, p = Predictor_Corrector((0, 20), 5000, falling_diff_k2, (0, 0))
    print '%.1f' % (p[-1][-1] / 75)
    print 'Safe, let\'s do it again'


def problem_2():
    t, x = Rounge_Kutta_4((0, 20), 10000, Lorenz_attractor, (0, 1, 1, 20))
    plt.figure('Time dependent plot of x, y, z')
    plt.plot(t, x[0, :])
    plt.plot(t, x[1, :])
    plt.plot(t, x[2, :])
    plt.xlabel('time [s]')
    plt.ylabel('Length [m]')
    plt.legend(['x', 'y', 'z'])
    fig = plt.figure('Lorenz attractor')
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(x[0, :].flatten(), x[1, :].flatten(), x[2, :].flatten())
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

    t, x = Rounge_Kutta_4((0, 20), 10000, Lorenz_attractor, (0, 0, 19, 2))
    plt.figure('Time dependent plot of x, y, z [modified]')
    plt.plot(t, x[0, :])
    plt.plot(t, x[1, :])
    plt.plot(t, x[2, :])
    plt.xlabel('time [s]')
    plt.ylabel('Length [m]')
    plt.legend(['x', 'y', 'z'])
    fig = plt.figure('Lorenz attractor [modified]')
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(x[0, :].flatten(), x[1, :].flatten(), x[2, :].flatten())
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')


def extra_work():
    leg = []
    for exponent in pl.frange(0, 5, 0.5):
        def Lane_Emden_exp(csi, x):
            return Lane_Emden(csi, x, exponent)
        t, x = Rounge_Kutta_4((0, 10), 100, Lane_Emden_exp, (0, complex(
            0., 0.0), complex(1., 0.0)), dtype=complex)
        plt.figure('')
        plt.plot(t, x[1, :])
        plt.ylim([-0.5, 1])
        plt.xlabel(r'$\xi$')
        plt.ylabel(r'$\theta$')
        leg.append('n = ' + str(exponent))
        plt.grid(True)
    plt.legend(leg, loc=3)

