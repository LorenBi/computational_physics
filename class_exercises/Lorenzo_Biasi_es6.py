import numpy as np
from copy import deepcopy
from tabulate import tabulate


# FUNCTIONS

def power_method(A, x, epsilon, lam=np.inf):
    new_x = np.dot(A, x)
    new_x = normalize(new_x)
    lambda_new = np.dot(np.transpose(new_x), np.dot(A, new_x))
    if abs(lam - lambda_new) < epsilon:
        return np.dot(A, new_x), float(lambda_new)
    else:
        return power_method(A, new_x, epsilon, lam=lambda_new)


# Some numerical functions
def normalize(x):
    return x / np.sqrt(sum([i ** 2 for i in x]))


def off(A):
    return Frobenius_norm(A - np.diag(A) * np.identity(len(A)))


def Frobenius_norm(A):
    return np.sqrt(np.trace(np.dot(np.transpose(A), A)))


def Cosine(chi):
    return np.sqrt((1. + chi) / 2.)


def Sine(chi, tau):
    return np.sign(tau) * np.sqrt((1. - chi) / 2.)


# Returns the sine and cosine needed for the trasfomation
def get_cos_sin(A, index):
    j, k = index
    if A[k, k] == A[j, j]:
        return np.sqrt(2) / 2., np.sqrt(2) / 2
    else:
        tau = (2 * A[j, k]) / (A[k, k] - A[j, j])
        chi = 1 / np.sqrt(1 + tau ** 2)
        return Cosine(chi), Sine(chi, tau)


# It takes a squared matrix A (from numpy.matrix) and it diagonalizes it with
# the Jacobi method. It cycles untill off(A) <= epsilon. The variable 'cycle'
# is to be set to true if you want to use the cycle jacobi method, false if
# you prefer the classical one.
def Jacobi_method(A, epsilon, cycle=True):
    R = np.identity(len(A))
    indexies = get_cycled_indexies(len(A))
    while off(A) > epsilon:
        index = find_next_index(A, indexies, cycle)
        C, S = get_cos_sin(A, index)
        make_diag_with_rotation(A, index, C, S)
        adjourn_rotation(R, index, C, S)
    return A, R


# It accounts for the possibility of the 'classical' Jacobi method or the
# one
def find_next_index(A, indexies, cycle):
    if cycle:
        index = indexies.pop(0)
        indexies.append(index)
    else:
        index = find_max_between(A, indexies)
    return index


# Just returns the indexies on which to cycle on for a nxn matrix
def get_cycled_indexies(n):
    indexies = []
    for i in range(n):
        for j in range(i + 1, n):
            indexies.append((i, j))
    return indexies


# Finds the index of the maximum element of a subsection of the matrix, given
# by the indexies
def find_max_between(A, indexies):
    max_value = - np.inf
    index = None
    for j, k in indexies:
        if abs(A[j, k]) > max_value:
            index = (j, k)
            max_value = abs(A[j, k])
    return index


# It rotates the matrix for putting a_jk and a_kj to 0
def make_diag_with_rotation(A, index, C, S):
    j, k = index
    a_jj = A[j, j]
    a_kk = A[k, k]
    a_jk = A[j, k]
    A[:, j] = C * A[:, j] - S * A[:, k]
    A[:, k] = S * A[j, :].reshape(np.shape(A[:, k])) + \
        C * A[k, :].reshape(np.shape(A[:, k]))
    A[j, :] = A[:, j].reshape(np.shape(A[j, :]))
    A[k, :] = A[:, k].reshape(np.shape(A[k, :]))
    A[j, j] = C * C * a_jj + S * S * a_kk - 2 * S * C * a_jk
    A[j, k] = 0
    A[k, j] = 0
    A[k, k] = S * S * a_jj + C * C * a_kk + 2 * S * C * a_jk


# OUTPUT FUNCTIONS

# It adjourns the rotation matrix that at the end will have the eigenvectors
# as columns
def adjourn_rotation(R, index, C, S):
    j, k = index
    R_rj = deepcopy(R[:, j])
    R[:, j] = C * R_rj - S * R[:, k]
    R[:, k] = S * R_rj + C * R[:, k]


# The next two functions are implements the previous two methods with some
# added features for the problems given
def power_method_print(A, x, epsilon, lam=np.inf, itera=0):
    new_x = np.dot(A, x)
    new_x = normalize(new_x)
    lambda_new = np.dot(np.transpose(new_x), np.dot(A, new_x))
    if itera % 2 == 1:
        print 'iteration:', itera, ' lambda:' '%.6f' % lambda_new
    if abs(lam - lambda_new) < epsilon:
        return np.dot(A, new_x), float(lambda_new), itera
    else:
        itera += 1
        return power_method_print(A, new_x, epsilon, lam=lambda_new, itera=itera)


def Jacobi_method_print(A, epsilon, cycle=True):
    R = np.identity(len(A))
    indexies = get_cycled_indexies(len(A))
    itera = 0
    table = [[itera, 'NA', 0, Frobenius_norm(A) ** 2, off(A) ** 2]]
    while off(A) > epsilon:
        index = find_next_index(A, indexies, cycle)
        j, k = index
        C, S = get_cos_sin(A, index)
        if A[k, k] == A[j, j]:
            theta_jk = np.pi / 4
        else:
            theta_jk = np.arctan((2 * A[j, k]) / (A[k, k] - A[j, j])) / 2.
        make_diag_with_rotation(A, index, C, S)
        adjourn_rotation(R, index, C, S)
        itera += 1
        table.append([itera, index, theta_jk / np.pi,
                      Frobenius_norm(A) ** 2, off(A) ** 2])
    return table, A, R


def problem_1():
    print 'Problem 1:\n'
    'Lambdas for each odd iteration'
    A = np.matrix([[1, 1, .5], [1, 1, .25], [.5, .25, 2]], dtype=float)
    eigenvec, max_eigenval, itera = power_method_print(
        A, np.array([[1], [0], [0]]), 10 ** -5, lam=np.inf)
    print '\n'
    print 'The eigenvalue found:', max_eigenval
    print 'The number of iteration', itera


def problem_2():
    print 'Problem 2:\n'
    A = np.matrix([[1, 1, .5], [1, 1, .25], [.5, .25, 2]], dtype=float)
    table, D, R = Jacobi_method_print(A, 10 ** -5, cycle=True)
    head = ['Iteration', '(j,k)', 'theta_jk/pi', 'Frobenius norm', 'off(A)^2']
    print tabulate(table, headers=head, floatfmt=".6f")


problem_1()
print '\n\n'
problem_2()
