import numpy as np
from tabulate import tabulate

# FIRT PROBLEM


# The function that we want to integrate
def f(x):
    return np.exp(x) * np.cos(x)


# The same function of before with the right change of variable for gauss-L
def f_gauss(x):
    arg = (np.pi / 2) * (x + 1)
    return (np.pi / 2) * np.exp(arg) * np.cos(arg)


# The trapezoid method
def trap_integral(f, interval):
    a, b = interval
    return abs(a - b) * (f(a) + f(b)) / 2.0


# The simpson 1/3 method
def simpson_1_3(f, interval):
    a, b = interval
    h = abs(a - b) / 2
    return (h / 3.0) * (f(a) + 4 * f((a + b) / 2) + f(b))


# The simpson 3/8 method
def simpson_3_8(f, interval):
    a, b = interval
    factor = (abs(a - b) * (3.0 / 8.0))
    x_1 = (2.0 * a + b) / 3.0
    x_2 = (a + 2.0 * b) / 3.0
    return factor * (f(a) + 3.0 * f(x_1) + 3.0 * f(x_2) + f(b)) / 3


# The composed method. BE CAREFUL N is not the total number of subintervals,
# but the number of intervals of which is applied the method chosen.
# for example, if we choose simpson 1/3, that function will be applied N times
# thus creating 2N subintervals. This was done for creating a more flexible
# function (and more readable).
def composed_method(f, interval, N, method=trap_integral):
    a, b = interval
    h = (b - a) / N
    start = 0
    integral = 0
    for i in range(N):
        integral += method(f, (start, start + h))
        start += h
    return integral


# The gauss legandre method. It doesn't need a variable N, as it's implicit
# in the vector point_weight that coitains the point and weight needed.
def gauss_legendre(f_gauss, point_weight):
    integral = 0
    for point, weight in point_weight:
        integral += f_gauss(point) * weight
    return integral


def read_and_get_gauss(file_name):
    file = open(file_name)
    lst = []
    for line in file:
        lst.append((float(string) for string in line.split()))
    return lst
# 'gauss_legendre/gauss_100_pontos.dat'


print 'Problem 1'
print '1.1'
head = ['Trapezio', 'Simpson 1/3', 'Simpson 3/8', 'Exacto']
print tabulate([[trap_integral(f, (0, np.pi)), simpson_1_3(f, (0, np.pi)), simpson_3_8(f, (0, np.pi)), -12.070346]], headers=head, floatfmt=".6f")
print '\n \n'
print '1.2'
Ns = [2, 4, 8, 10, 20, 30, 50, 100]
interval = (0, np.pi)
table = [[]]
interval = (0, np.pi)
for N in Ns:
    table[-1].append(N)
    table[-1].append(composed_method(f, interval, N))
    # It can be seen that here we use N / 2 instead on N, since the definition
    # interval in class was different of how this function is implemented
    table[-1].append(composed_method(f, interval, N / 2, method=simpson_1_3))
    point_weight = read_and_get_gauss('gauss_legendre/gauss_' + str(N) + '_pontos.dat')
    table[-1].append(gauss_legendre(f_gauss, point_weight))
    table[-1].append(-12.070346)
    table.append([])

head = ['N', 'Trapezio', 'Simpson 1/3', 'Gauss-Legendre', 'Exacto']
print tabulate(table, headers=head, floatfmt=".6f")


# SECOND PROBLEM

# the function we want to use
def f_2(x):
    return np.exp(x) * np.sin(x) + np.exp(-x) * np.cos(x)


# its derivative
def d_f_2(x):
    return (np.exp(x) - np.exp(-x)) * (np.sin(x) + np.cos(x))


# A general function that uses various method to derive
def derive(f, x, N_points, h):
    if N_points == 2:
        return derive_2(f, x, h)
    elif N_points == 3:
        return derive_3(f, x, h)
    elif N_points == 5:
        return derive_5(f, x, h)


# The next 3 function are pretty self explanatory
def derive_2(f, x, h):
    return (f(x + h) - f(x)) / h


def derive_3(f, x, h):
    return (- f(x + 2 * h) + 4 * f(x + h) - 3 * f(x)) / (2 * h)


def derive_5(f, x, h):
    upper = - 3 * f(x + 4 * h) + 16 * f(x + 3 * h) - 36 * f(x + 2 * h) + 48 * f(x + h) - 25 * f(x)
    return upper / (12 * h)


print '\nProblem 2'

# Just prepering the output and formatting it
xs = [0, np.pi / 4, np.pi / 2, 3 * np.pi / 4, np.pi]
hs = [0.1, 0.05, 0.01]
head = ['x', 'h', '2 points', '3 points', '5 points', 'Exact']
point_number = [2, 3, 5]
table1 = [[]]
for x in xs:
    for h in hs:
        table1[-1].append(x)
        table1[-1].append(h)
        for N in point_number:
            table1[-1].append(derive(f_2, x, N, h))
        table1[-1].append(d_f_2(x))
        table1.append([])
    table1.append([])

print tabulate(table1, headers=head, floatfmt=".6f")

lista = read_and_get_gauss('gauss_legendre/gauss_4_pontos.dat')
print [element for element in lista]
