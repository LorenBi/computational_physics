import numpy as np
from math import sqrt, exp
from random import random
import matplotlib.pyplot as plt


def armonic_local_energy(x, alpha):
    return alpha ** 2 + (x ** 2) * (1 - alpha ** 4)


def gauss_local_energy(x, alpha):
    return (alpha ** 2) - (x ** 2) * (alpha ** 4) - 5 * exp(- x ** 2)


def armonic_wave_fun(x, alpha):
    return exp(-(alpha * x) ** 2 / 2)  # alpha / sqrt(pi) *


def quantum_MC(wave_and_energy, N_MC, N_term, position, alpha):
    wave_function, local_energy = wave_and_energy
    sum_energy = 0
    sum_energy_squared = 0
    x, dx = position

    for i in range(N_MC):
        x = termalize(x, dx, alpha, wave_function, N_term)
        sum_energy += local_energy(x, alpha)
        sum_energy_squared += local_energy(x, alpha) ** 2

    mean_E = sum_energy / float(N_MC)
    mean_E_sq = sum_energy_squared / float(N_MC)
    std_E = sqrt((mean_E_sq - mean_E ** 2) / float(N_MC))

    return mean_E, mean_E_sq, std_E


def correlated_sampling(wave_and_energy, xs, alpha, beta):
    wave_function, local_energy = wave_and_energy
    S1 = S2 = S3 = 0
    for x in xs:
        wave_ratio = (wave_function(x, alpha) / wave_function(x, beta)) ** 2
        E_L = local_energy(x, alpha)
        S1 += E_L * wave_ratio
        S2 += E_L ** 2 * wave_ratio
        S3 += wave_ratio
    mean_E = S1 / S3
    mean_E_sq = S2 / S3
    std_E = sqrt((mean_E_sq - mean_E ** 2) / float(len(xs)))

    return mean_E, mean_E_sq, std_E


def termalize(x, dx, alpha, wave_function, N_term):
    for i in range(N_term):
        x_prop = x + (random() - 0.5) * dx
        weight = (wave_function(x_prop, alpha) / wave_function(x, alpha)) ** 2
        if weight > random():
            x = x_prop
    return x


def create_sample_base(wave_and_energy, N_MC, N_term, position, beta):
    wave_function, local_energy = wave_and_energy
    x, dx = position
    xs = []
    for i in range(N_MC):
        xs.append(termalize(x, dx, beta, wave_function, N_term))
    return xs


def calc_plot_energies(N_MC, N_term, w_and_e, alphas, position, name):
    energy = np.empty(alphas.shape)
    d_energy = np.empty(alphas.shape)
    for i, alpha in enumerate(alphas):
        mean_E, mean_E_sq, std_E = quantum_MC(
            w_and_e, N_MC, N_term, position, alpha)
        energy[i] = mean_E
        d_energy[i] = std_E
    plt.figure()
    plt.errorbar(alphas, energy, yerr=d_energy, fmt=' ')
    plt.grid(True, linestyle='-')
    plt.xlabel(r'Parameter $\alpha$')
    plt.ylabel(r'E[$\Psi_T$]')
    plt.title('Energies plot: ' + name)


def plot_correlated(N_MC, N_term, w_and_e, alphas, position, beta):
    energy = np.empty(alphas.shape)
    d_energy = np.empty(alphas.shape)
    xs = create_sample_base(w_and_e, N_MC, N_term, position, beta)
    for i, alpha in enumerate(alphas):
        mean_E, mean_E_sq, std_E = correlated_sampling(
            w_and_e, xs, alpha, beta)
        energy[i] = mean_E
        d_energy[i] = std_E
    plt.figure()
    plt.errorbar(alphas, energy, yerr=d_energy, fmt=' ')
    plt.grid(True, linestyle='-',)
    plt.xlabel(r'Parameter $\alpha$')
    plt.ylabel(r'E[$\Psi_T$]')
    plt.title('Correlated plot')
    print 'Optimal value for alpha is:', alphas[np.argmin(energy)]
    print 'Eigenvalue for the energy:', np.min(energy)


def problem_1():
    w_and_e = armonic_wave_fun, armonic_local_energy
    N_MC = 1000
    N_term = 10
    position = (4., 2.)
    alphas = np.arange(.3, 2, 0.05)
    calc_plot_energies(N_MC, N_term, w_and_e, alphas, position, 'armonic')


def problem_2():
    w_and_e = armonic_wave_fun, gauss_local_energy
    N_MC = 1000
    N_term = 10
    position = (1.5, 2.)
    alphas = np.arange(0.5, 2, 0.05)
    calc_plot_energies(N_MC, N_term, w_and_e, alphas, position, 'exponential')
    plot_correlated(N_MC, N_term, w_and_e, alphas, position, 1.25)


if __name__ == '__main__':
    plt.rc('text', usetex=True)
    problem_1()
    problem_2()
    plt.show()
