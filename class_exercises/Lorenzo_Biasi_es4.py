import numpy as np
from numpy import array


# THE MAIN FUNCTIONS

# It creates the hilbert matrix
def solve_linear(A, b, pivot=False):
    return backward_subs(*gauss_elimination(A, b))


def det(A, pivot=False):
    # I use the gauss elimination adding an empty array
    U, c = gauss_elimination(A, np.zeros((np.shape(A)[0], 1)), pivot=pivot)
    return np.prod(U.diagonal())


def gauss_elimination(A, b, pivot=False):
    A = np.hstack((A, b))
    for j in range(np.shape(A)[0] - 1):
        partial_pivot(A, j) if pivot else None
        # Here I remove to the the rows from j+1 the row j multiplied by the
        # elements on j column divided by the A[j,j]. Besically this is the
        # algorithm that needs to be repeted n - 1 times
        A[j + 1:][:] = A[j + 1:][:] - np.tile(A[j: j + 1], np.shape(
            A[j + 1:, 0][:, None])) * ((A[j + 1:, j][:, None] / A[j, j]))
    return A[:, 0:-1], A[:, -1:]


def partial_pivot(A, index_pivot):
    next_pivot = A[index_pivot:, index_pivot].argmax()
    if next_pivot != 0:
        print next_pivot
        # here I use advance slicing to swap the lines
        A[[index_pivot, index_pivot + next_pivot],
            :] = A[[index_pivot + next_pivot, index_pivot], :]


def backward_subs(A, c):
    x = np.empty(len(c))
    x[-1] = c[-1] / A[-1][-1]
    for k in range(len(c) - 2, -1, -1):
        x[k] = (c[k] - np.sum(A[k, k + 1:] * x[k + 1:])) / A[k][k]
    return x


def create_hilbert(n):
    H = np.empty([n, n])
    for i in range(1, n + 1):
        for j in range(1, n + 1):
            H[i - 1][j - 1] = 1. / (i + j - 1)
    return H


# THE OUTPUT
def gauss_print(A, b):
    A = np.hstack((A, b))
    print A
    print'\n Iteration:', 0
    print 'A\n', A[:, 0:-1]
    print 'b\n', A[:, -1:]
    for j in range(np.shape(A)[0] - 1):
        A[j + 1:][:] = A[j + 1:][:] - np.tile(A[j: j + 1], np.shape(
            A[j + 1:, 0][:, None])) * ((A[j + 1:, j][:, None] / A[j, j]))
        print'\n Iteration:', j + 1
        print 'A\n', A[:, 0:-1]
        print 'b\n', A[:, -1:]
    return A[:, 0:-1], A[:, -1:]


def esercicio_3():
    A = array([[1, 2, 1], [2, 3, 3], [-1, -3, 1]], dtype=float)
    inv_A = []
    for x in [array([1., 0., 0.]), array([0., 1., 0.]), array([0., 0., 1.])]:
        inv_A.append(list(solve_linear(A, x.reshape((3, 1)))))
    inv_A = np.transpose(np.array(inv_A))
    print 'Inverted A'
    print inv_A
    print 'A * A ** -1 = '
    print np.dot(A, inv_A)


def esercicio_2():
    H_3 = create_hilbert(3)
    print 'a)'
    print 'Determinant of Hilbert 3x3', det(H_3)
    print 'Solution of Hilbert 3x3', solve_linear(H_3, array(range(1, 4), dtype=float).reshape((3, 1)))
    print 'All intermidiate steps for gauss elimination'
    gauss_print(H_3, array(range(1, 4), dtype=float).reshape((3, 1)))
    H_5 = create_hilbert(5)
    print '\nb)'
    print 'Determinant of Hilbert 5x5', det(H_5)
    print 'Solution of Hilbert 5x5', solve_linear(H_5, array(range(1, 6), dtype=float).reshape((5, 1)))
    a, b = gauss_elimination(H_5, array(
        range(1, 6), dtype=float).reshape((5, 1)))
    print 'Final form of Hilbert 5x5 after gauss elimination'
    print a
    print b
    print '\nc)'
    print 'Determinant of Hilbert 10x10', det(create_hilbert(5))


print 'Execise one:\n \n'
esercicio_2()

print '\n \n \nExercise three:\n \n'
esercicio_3()
