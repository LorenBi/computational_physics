import Lorenzo_Biasi_es5 as LB5
import matplotlib.pyplot as plt
import numpy as np
from time import time
import matplotlib


def parabola(t, x):
    dx = x[1]
    ddx = - (np.pi ** 2) * (x[0] + 1) / 4.
    return np.array([dx, ddx])


def shooting_method(interval, N, diff_fun, initial_cond, epsilon, alpha):
    real = initial_cond[-1]
    t, x = adjourn_with_apha(interval, N, diff_fun, initial_cond, alpha[0])
    x_end = x[0, -1] - real
    while abs(x[0, -1] - real) > epsilon:
        t, x = adjourn_with_apha(interval, N, diff_fun, initial_cond, alpha[1])
        next_alpha = (alpha[0] * (x[0, -1] - real) -
                      alpha[1] * x_end) / (x[0, -1] - real - x_end)
        alpha.pop(0)
        alpha.append(next_alpha)
    print 'Guessed derivative in 0:', alpha[-1]
    return t, x


def adjourn_with_apha(interval, N, diff_fun, initial_cond, alpha_):
    initial_cond[-1] = alpha_
    return LB5.Rounge_Kutta_4(interval, N, diff_fun, initial_cond)


def initialize_netspace(Ns, intervals):
    x = []
    for N, interval in zip(Ns, intervals):
        x.append(np.linspace(*interval, num=N))
    return x


def initialize_heatspace(netspace, Ts):
    temperature = np.full((100, 100), 50.)
    temperature[0, :] = temperature[:, -1] = Ts[0]
    temperature[:, 0] = temperature[-1, :] = Ts[1]
    return temperature


def solve_laplace_3(x, f, epsilon, alpha=0.):
    too_far = 1
    f_new = np.full(f.shape, np.inf)
    iteration = 0
    while too_far:
        too_far = 0
        for col in range(1, f.shape[1] - 1):
            f_new[1:-1, col] = (f[1:-1, col - 1] + f[1:-1, col + 1]) / 4.
        for row in range(1, f.shape[0] - 1):
            f_new[row, 1:-1] += (f[row - 1, 1:-1] + f[row + 1, 1:-1]) / 4.
        f_new[1:-1, 1:-1] = f_new[1:-1, 1:-1] * \
            (1 - alpha) + alpha * f[1:-1, 1:-1]
        distances = abs(
            (f[1:-1, 1:-1] - f_new[1:-1, 1:-1]) / f_new[1:-1, 1:-1])
        too_far = sum(sum(distances > epsilon))
        if iteration % 100 == 0:
            print 'Number of iteration: ', iteration
        f[1:-1, 1:-1] = f_new[1:-1, 1:-1]
        iteration += 1
    return f, iteration


def problema_1():
    plt.figure('')
    interval = (0, 1)
    N = 10000
    initial_cond = [0, 0, 1]
    epsilon = 10 ** -5
    alpha = [0, 5.5]

    t, x = shooting_method(interval, N, parabola, initial_cond, epsilon, alpha)
    plt.xlabel('x')
    plt.ylabel('y(x)')
    plt.grid(True)
    plt.plot(t, x[0, :])


def problema_2():
    Ns = (100, 100)
    intervals = np.array([(0, 1), (0, 1)])
    x = initialize_netspace(Ns, intervals)
    T = initialize_heatspace(x, (0, 100))
    plt.figure()
    start = time()
    print solve_laplace_3(x, T, 5 * 10 ** -5)
    print 'Time: ', time() - start, 's'
    for row in range(1, 10):
        plt.plot(x[0], T[row * 10, :], label='y = ' + str(row * 0.1))
    plt.legend()
    plt.xlabel('Coordinate x [m]')
    plt.ylabel(r'Temperature [$^o$C]')
    plt.grid(True)


if __name__ == '__main__':
    matplotlib.style.use('seaborn-white')
    print 'Problem 1'
    problema_1()
    print '\n'
    print 'Problem 2'
    problema_2()
    plt.show()
