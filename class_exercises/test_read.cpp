#include<iostream>
#include<fstream>
#include <vector>


using namespace std;


void print_vector(vector<vector<double>>& S){
	for (unsigned int i = 0; i < S.size(); ++i)
	{
		for (unsigned int j = 0; j < S[i].size(); ++j)
		{

			cout << S[i][j] << " ";
		}
		cout << endl;
	}
}


void calculate_E_M(vector<vector<double>>& S, double * E, double * M){
	for (unsigned int i = 0; i < S.size(); ++i){for (auto& n : S[i]) *M += n;}

	for (unsigned int i = 0; i < S.size(); ++i)
	{
		for (unsigned int j = 0; j < S[i].size(); ++j)
		{
			if (i + 1 != S.size())
			{
				*E += - S[i][j] * S[i + 1][j];
			}
			if (j + 1 != S.size())
			{
				*E += - S[i][j] * S[i][j + 1];
			}
		}
	}
}


double calculate_energy_change(vector<vector<double>>& S, unsigned int i, unsigned int j){
	double dE;
    double s = S[i][j];
    if (i > 0){
        dE += 2 * s * S[i - 1][j];
    }
    if (i < S.size() - 1){
        dE += 2 * s * S[i + 1][j];
    }
    if (j > 0){
        dE += 2 * s * S[i][j - 1];
    }
    if (j < S[i].size() - 1){
        dE += 2 * s * S[i][j + 1];
    }
    return dE;
}




int main() {
 int i, j; double s, E, M;
 ifstream myReadFile;
 myReadFile.open("exemplo-0-19.dat");

 vector<vector<double>> S(20, vector<double>(20));
 if (myReadFile.is_open()) {
 while (!myReadFile.eof()) {
    myReadFile >> i;
    myReadFile >> j;
    myReadFile >> s;
    S[i][j] = s;
 }
}
cout << S[0][0];
myReadFile.close();
i = 0; j = 0;
cout << i << j << ": " << calculate_energy_change(S, i, j) << endl;
i = 0; j = 12;
cout << i << j << ": " << calculate_energy_change(S, i, j) << endl;
i = 14; j = 14;
cout << i << j << ": " << calculate_energy_change(S, i, j) << endl;
i = 10; j = 10;
cout << i << j << ": " << calculate_energy_change(S, i, j) << endl;
i = 7; j = 9;
cout << i << j << ": " << calculate_energy_change(S, i, j) << endl;
i = 5; j = 11;
cout << i << j << ": " << calculate_energy_change(S, i, j) << endl;
calculate_E_M(S, &E, &M);
cout << E << " " << M;
return 0;
}

