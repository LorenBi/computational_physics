import math
import sys

# There are 2 main function one implements Newton_Raphson
# Find zero needs to have an added string to set the method
# 'BI' = bisection method
# 'SC' = secant method
# 'RF' = Regula Falsi


NITMAX = 10 ** 5  # Numeber of iteration
DELTA = 10 ** -4  # Tollerance accepted from 0
ADDED_BOO = False  # True if you want the condition |x{n+1} - x{n}| < EPSILON
EPSILON = 10 ** -6  # Distance between |x{n+1} - x{n}|

sys.setrecursionlimit(NITMAX)


def f(x):
    return math.sin(x)


def derivative_f(x):
    return math.cos(x)


def Newton_Raphson(x, x_before=False, i=0):
    i += 1
    boolean = ADDED_BOO and False is not x_before
    if abs(f(x)) < DELTA and abs(x - x_before) < EPSILON and boolean:
        return x, i, abs(x - x_before)
    elif abs(f(x)) < DELTA and not boolean:
        return x, i
    else:
        return Newton_Raphson(x - f(x) / derivative_f(x), x_before=x, i=i)


def find_zero(x_n, x_n1, method, i=0):
    i += 1
    x_n, x_n1 = find_next_interval(x_n, x_n1, method)
    if abs(f(x_n1)) < DELTA and abs(x_n - x_n1) < EPSILON and ADDED_BOO:
        return x_n1, i, abs(x_n - x_n1)
    elif abs(f(x_n1)) < DELTA and not ADDED_BOO:
        return x_n1, i
    else:
        return find_zero(x_n, x_n1, method, i=i)


def find_next_interval(x_n, x_n1, method):
    if method == 'BI':
        return bisection_interval(x_n, x_n1, method)
    elif method == 'RF':
        return regula_falsi(x_n, x_n1, method)
    elif method == 'SC':
        return secante(x_n, x_n1, method)


def bisection_interval(x_n, x_n1, method):
    x_next = float(x_n1 + x_n) / 2
    if f(x_next) * f(x_n1) <= 0:
        return x_n1, x_next
    else:
        return x_n, x_next


def regula_falsi(x_n, x_n1, method):
    x_next = (x_n * f(x_n1) - x_n1 * f(x_n)) / (f(x_n1) - f(x_n))
    if f(x_next) * f(x_n1) <= 0:
        return x_n1, x_next
    else:
        return x_n, x_next


def secante(x_n, x_n1, method):
    x_next = (x_n * f(x_n1) - x_n1 * f(x_n)) / (f(x_n1) - f(x_n))
    return x_n1, x_next

# THE OUTPUT WILL BE HERE

print 'The points found without setting a condition on the distance between x and x from before'
for method in ['BI', 'RF', 'SC']:
    print method + ' ' + str(find_zero(2, 4, method)[1]) + ' ' + str(find_zero(2, 4, method)[0]) + ' ' + str(f(find_zero(2, 4, method)[0]))
print 'NR' + ' ' + str(find_zero(2, 4, method)[1]) + ' ' + str(Newton_Raphson(4, method)[0]) + ' ' + str(f(Newton_Raphson(4, method)[0]))

print '\n'
ADDED_BOO = True
print 'The points found setting a condition on the distance between x and x from before'
for method in ['BI', 'RF', 'SC']:
    print method + ' ' + str(find_zero(2, 4, method)[1]) + ' ' + str(find_zero(2, 4, method)[0]) + ' ' + str(f(find_zero(2, 4, method)[0])) + ' ' + str(find_zero(2, 4, method)[2])
print 'NR' + ' ' + str(find_zero(2, 4, method)[1]) + ' ' + str(Newton_Raphson(4, method)[0]) + ' ' + str(f(Newton_Raphson(4, method)[0])) + ' ' + str(find_zero(2, 4, method)[2])
