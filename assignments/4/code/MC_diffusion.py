import numpy as np
from math import log, exp
import matplotlib.pyplot as plt
import random
from copy import deepcopy


# I created an object that  would run our monte carlo diffusion algorithm
class MC_diffusion:
    # initialize the problem
    def __init__(self, V, N_t, N_term, dt, E_R, alpha):
        self.V = V
        self.N_t = N_t
        self.N_term = N_term
        self.dt = dt
        self.E_R = E_R
        self.alpha = alpha
        self.SE = 0
        self.SE2 = 0
        self.SE2
        self.Es = np.empty(N_t)

    # set the walkers to the needed distribution
    def set_walkers(self, walkers):
        self.walkers = walkers
        self.N_p = self.N = len(self.walkers)

    # run the general algorithm and save the requested values
    def solve(self):
        for t in range(self.N_t):
            self.termalize()
            self.SE += self.E_R
            self.SE2 += self.E_R ** 2
            self.Es[t] = self.SE / (t + 1)
            if t == 0:
                self.walkers_first_iter = deepcopy(self.walkers)
            if t % 100 == 0:
                print t

        float_N_t = float(self.N_t)
        self.E = self.SE / float_N_t
        self.E2 = self.SE2 / float_N_t
        self.dE = ((self.E2 - self.E ** 2) / float_N_t) ** .5

    def termalize(self):
        for term in range(self.N_term):
            self.walkers += np.random.normal(0, 1, self.N) * self.dt ** .5

            # This line is for generating random numbers with rejection
            # accepting.
            # I don't use it as it is rather slow.

            # self.walkers += rejection_acception(self.N) * self.dt ** .5
            q = np.exp(-(self.V(self.walkers) - self.E_R) * self.dt)
            n = np.trunc(q + np.random.random(self.N)).astype(int)

            self.N = sum(n)
            self.walkers = np.repeat(self.walkers, n)

            if self.N != self.N_p:
                self.E_R += self.alpha * log(self.N_p * 1. / self.N)


def gauss(x):
    return exp(- (x * x) / 2)


def rejection_acception(N):
    x = np.empty(N)
    i = 0
    while i < N:
        # I use a uniform -2 2, it should be more or less enough
        # it can be changed depending on the need
        s = random.uniform(-2, 2)
        r = random.random()
        if r < gauss(s):
            x[i] = s
            i += 1
    return x


def problem_6_to_10():
    N_t = 2000
    N_p = 3000

    # Plot the distribution of walkers
    plt.figure()
    walkers = np.random.uniform(-1, 1, 3000)
    plt.hist(walkers)
    plt.grid(linestyle='dotted')
    plt.xlabel('x')
    plt.ylabel('counts')
    plt.savefig('uniform.jpg')


    for distribution in ['uniform', 'in_0']:
        # This is where you initilize the walkers distribution
        problem = MC_diffusion(lambda x: (x * x) / 2., N_t, 10, .01, 0, .3)
        if distribution == 'uniform':
            problem.set_walkers(walkers)
        elif distribution == 'in_0':
            problem.set_walkers(np.zeros(N_p))

        problem.solve()
        plt.figure()

        # Here I create two lists for creating the histogram
        # bins divides the space in which the walkers sit
        # counts is the number of walkers in each bin
        counts, bins = np.histogram(problem.walkers, 60)
        # Since we work with QM we need to normalize properly
        # np.diff is nothing other then the spacing of the bins
        N2 = sum(counts * counts * np.diff(bins)) ** .5
        plt.plot(bins[:-1] + np.diff(bins) / 2, counts / N2, marker='.', label='Final result')

        counts, bins = np.histogram(problem.walkers_first_iter, 60)
        N2 = sum(counts * counts * np.diff(bins)) ** .5
        plt.plot(bins[:-1] + np.diff(bins) / 2, counts / N2, marker='.', label='After one time step')

        x = np.linspace(-4, 4, 100)
        plt.plot(x, np.exp(-x ** 2 / 2.) / np.pi ** .25, color='orange', label='Analytical result')

        plt.legend()
        plt.xlabel('x')
        plt.ylabel('wave function')

        plt.savefig('wave_function' + distribution + '.jpg')

        # plot the energy over time
        plt.figure()
        plt.plot(range(N_t), problem.Es)
        plt.xlabel('Iteration')
        plt.ylabel('<E_R>')
        plt.savefig('energy_time' + distribution + '.jpg')

        print 'Energy for ' + distribution + 'distribution', problem.E, '+-', problem.dE


problem_6_to_10()
