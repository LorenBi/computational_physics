import numpy as np
import matplotlib.pyplot as plt
from itertools import product
from copy import deepcopy


# This function takes an numpy.array and return two numpy array
# the input is the distances and the output is the correlated  forces and energy
def Lennard_force(dr):
    # interaction is a global boolean variable
    if interaction:
        r = np.sum(dr ** 2, axis=1) ** .5
        F = 24. / r**8 * (2. / r ** 6 - 1)
        u = 4 * ((1. / r) ** 12 - (1. / r) ** 6)
        return np.stack((F * dr[:, 0], F * dr[:, 1]), axis=-1), u
    else:
        return np.stack((0 * dr[:, 0], 0 * dr[:, 1]), axis=-1), np.zeros(dr.shape[0])


# It computes the distances between the particles using a periodic boundary condition
def compute_dist(r, Ls, part):
    dr = np.empty(r[part + 1:, :].shape)
    for col in range(len(Ls)):

        L = Ls[col]
        dr[:, col] = r[part, col] - r[part + 1:, col]
        dr[dr[:, col] > L / 2., col] = dr[dr[:, col] > L / 2., col] - L
        dr[dr[:, col] <= - L / 2., col] = dr[dr[:, col] <= - L / 2., col] + L

    return dr


# It calculates the acceleration for each particle
def accelALL(r, acc, Ls):
    acc.fill(0)
    N, dim = r.shape
    E_pot = virial = 0
    # since I work with array there is no need to loop twice, as it is done implicitly
    for i in range(N - 1):
        dr = compute_dist(r, Ls, i)
        f, u = Lennard_force(dr)
        acc[i, :] += np.sum(f, axis=0)
        acc[i + 1:, :] += -f  # 3rd law of newton Fij = -Fji

        E_pot += sum(u)
        virial += np.sum(dr * f)
    return E_pot, virial


# It adjourns the position using an intermediate time
def velverALL(dt, r, v, acc, Ls):
    v += acc * dt / 2.
    r += v * dt
    r = np.remainder(r, Ls)  # It makes sure r is in the box
    E_pot, virial = accelALL(r, acc, Ls)
    v += acc * dt / 2.
    E_kin = np.sum(v ** 2) / 2.
    return E_kin, E_pot, virial


def molecular_dyn(r, v, Nt, dt, Ls):
    global flag
    if flag:  # This is needed  in the 5th problem
        x = np.empty((r.shape[0], Nt))
        y = np.empty((r.shape[0], Nt))
    else:
        x = np.empty((3, Nt))
        y = np.empty((3, Nt))

    # I initialize the variables
    E_pots = np.empty(Nt + 1)
    E_kins = np.empty(Nt + 1)
    virials = np.empty(Nt + 1)
    v_cm = np.empty((Nt + 1, 2))
    acc = np.zeros(r.shape)
    E_pot, virial = accelALL(r, acc, Ls)

    E_kins[0] = np.sum(v ** 2) / 2.
    E_pots[0] = E_pot
    virials[0] = virial
    v_cm[0, :] = np.sum(v, axis=0) / 16.


    for it in range(Nt):

        E_kin, E_pot, virial = velverALL(dt, r, v, acc, Ls)
        r = np.remainder(r, Ls)
        if flag:  # This is needed  in the 5th problem
            x[:, it] = r[:, 0]
            y[:, it] = r[:, 1]

        else:
            x[:, it] = r[[7, 5, 20], 0]
            y[:, it] = r[[7, 5, 20], 1]
            if it == 99:
                r_old = deepcopy(r)

        E_pots[it + 1] = E_pot
        E_kins[it + 1] = E_kin
        v_cm[it + 1, :] = np.sum(v, axis=0) / 16.
        virials[it + 1] = virial

    if not flag:  # This is needed  in the 5th problem
        return x, y, v, E_pots, E_kins, virials, v_cm, r, r_old

    return x, y, v, E_pots, E_kins, virials, v_cm


def problem_1():
    data = np.genfromtxt('init.dat')
    r = data[:, :2]
    v = data[:, 2:]
    Nt = 200
    dt = 0.01
    Ls = [6., 6.]

    plt.figure()
    global interaction # this removes or adds interactions
    interaction = True
    x, y, v, E_pots, E_kins, virials, v_cm = molecular_dyn(r, v, Nt, dt, Ls)
    colors = ['blue', 'orange', 'green', 'red']
    for i, col in zip([4, 5, 6, 9], colors):
        # do each plot with the interactions
        plt.plot(x[i, :], y[i, :], label='Particle: ' + str(i + 1), color=col)

    # The non interactive part
    r = data[:, :2]
    v = data[:, 2:]
    interaction = False
    x, y, v, E_pots, E_kins, virials, v_cm = molecular_dyn(r, v, Nt, dt, Ls)
    for i, col in zip([4, 5, 6, 9], colors):
        # this 5 lines needs to cut the lines in the plot
        # when the particles cross the border
        a = np.abs(np.diff(y[i, :])) >= 0.5
        b = np.abs(np.diff(x[i, :])) >= 0.5
        pos = np.where(np.logical_or(a, b))[0]
        x[i, pos] = np.nan
        y[i, pos] = np.nan
        plt.plot(x[i, :], y[i, :], linestyle='dotted', color=col)
    plt.legend()
    plt.xlabel('x [m]')
    plt.ylabel('y [m]')
    plt.grid(linestyle='dotted')
    plt.savefig('fig_problem_1.jpg')



def problem_2():
    data = np.genfromtxt('init.dat')
    r = data[:, :2]
    v = data[:, 2:]
    Nt = 1000
    dt = 0.01
    Ls = [6., 6.]

    plt.figure()
    x, y, v, E_pots, E_kins, virials, v_cm = molecular_dyn(r, v, Nt, dt, Ls)
    for i in [4, 5]:
        plt.plot(x[i, :], y[i, :])

    # create the 'final.dat' file
    position = np.hstack((x[:, -1].reshape(16, 1), y[:, -1].reshape(16, 1)))
    np.savetxt('final.dat', np.hstack((position, v)), delimiter='\t')

    # Time inversion
    data_f = np.genfromtxt('final.dat')
    r = data_f[:, :2]
    v = data_f[:, 2:]

    x, y, v, E_pots, E_kins, virials, v_cm = molecular_dyn(r, -v, Nt, dt, Ls)
    for i in [4, 5]:

        plt.plot(x[i, :], y[i, :], label='Particle: ' + str(i + 1), marker='')
    plt.legend()
    plt.grid(linestyle='dotted')
    plt.xlabel('x [m]')
    plt.ylabel('y [m]')
    plt.savefig('fig_problem_2a')

    # do the same as previously just with more time steps
    data = np.genfromtxt('init.dat')
    r = data[:, :2]
    v = data[:, 2:]
    Nt = 2000

    plt.figure()
    x, y, v, E_pots, E_kins, virials, v_cm = molecular_dyn(r, v, Nt, dt, Ls)
    for i in [4, 5]:
        # this 5 lines needs to cut the lines in the plot
        # when the particles cross the border
        a = np.abs(np.diff(y[i, :])) >= 0.5
        b = np.abs(np.diff(x[i, :])) >= 0.5
        pos = np.where(np.logical_or(a, b))[0]
        x[i, pos] = np.nan
        y[i, pos] = np.nan
        plt.plot(x[i, :], y[i, :], label='Time reversed particle: ' + str(i))

    r = np.hstack((x[:, -1].reshape((16, 1)), y[:, -1].reshape((16, 1))))

    x, y, v, E_pots, E_kins, virials, v_cm = molecular_dyn(r, -v, Nt, dt, Ls)
    for i in [4, 5]:
        # this 5 lines needs to cut the lines in the plot
        # when the particles cross the border
        a = np.abs(np.diff(y[i, :])) >= 0.5
        b = np.abs(np.diff(x[i, :])) >= 0.5
        pos = np.where(np.logical_or(a, b))[0]
        x[i, pos] = np.nan
        y[i, pos] = np.nan
        plt.plot(x[i, :], y[i, :], label='Particle: ' + str(i), marker='')
    plt.legend()
    plt.grid(linestyle='--')
    plt.xlabel('x [m]')
    plt.ylabel('y [m]')
    plt.savefig('fig_problem_2b')


def problem_3():
    global interaction
    # a loop over Nt and dt so that we can calculate for both conditions
    for Nt, dt in zip([100, 1000], [0.01, 0.001]):
        data = np.genfromtxt('init.dat')
        r = data[:, :2]
        v = data[:, 2:]
        print 'Initial v_cm', np.sum(v, axis=0)
        L = [6., 6.]

        # remove the interactions and do the calculations
        interaction = False
        x, y, v, E_pots, E_kins, virials, v_cm = molecular_dyn(r, v, Nt, dt, L)
        E_tot = E_kins + E_pots
        E_rel_no_int = ((E_tot) - (E_tot)[0]) / (E_tot)[0]
        v_cm_no_int = np.sum(v_cm ** 2, axis=1) ** .5
        data = np.genfromtxt('init.dat')
        r = data[:, :2]
        v = data[:, 2:]

        # put back the interactions and see the difference
        interaction = True
        x, y, v, E_pots, E_kins, virials, v_cm = molecular_dyn(r, v, Nt, dt, L)
        E_tot = E_kins + E_pots
        E_rel = ((E_tot) - (E_tot)[0]) / (E_tot)[0]

        plt.figure()
        plt.plot(np.arange(Nt + 1) * dt, E_rel, label='With interactions')
        plt.plot(np.arange(Nt + 1) * dt, E_rel_no_int, label='With no interactions')
        plt.grid(linestyle='--')
        plt.xlabel('time [s]')
        plt.ylabel(r'$\eta(t)$', fontsize=14)
        plt.legend()
        plt.savefig('rel_energy' + str(Nt) + '.jpg')

        plt.figure()
        v_cm = np.sum(v_cm ** 2, axis=1) ** .5
        plt.plot(np.arange(Nt + 1) * dt, v_cm, label='With interactions')
        plt.plot(np.arange(Nt + 1) * dt, v_cm_no_int, label='With no interactions')
        plt.grid(linestyle='--')
        plt.xlabel('time [s]')
        plt.ylabel(r'$\left\lVert v_{cm} \right\rVert \text{[m / s]}$', fontsize=14)
        plt.legend()
        plt.savefig('momentum' + str(Nt) + '.jpg')

        print 'Nt:', Nt
        print 'Maximum relative energy: ', max(E_rel, key=abs)
        print 'Maximum momentum deviation: ', max(v_cm - v_cm[0], key=abs)
        print '\n'


def problem_4():
    data = np.genfromtxt('init.dat')
    r = data[:, :2]
    v = data[:, 2:]
    v -= np.sum(v, axis=0) / 16.  # remove momentum of the center of mass
    N, dim = r.shape
    Nt = 10000
    dt = 0.01
    Ls = [6., 6.]
    V = float(Ls[0] * Ls[1])

    x, y, v, E_pots, E_kins, virials, v_cm = molecular_dyn(r, v, Nt, dt, Ls)
    E_kins_time = np.empty(Nt + 1)
    E_pots_time = np.empty(Nt + 1)
    virial_time = np.empty(Nt + 1)
    for i in range(Nt + 1):  # do the time average
        E_kins_time[i] = sum(E_kins[:i + 1]) / (i + 1)
        E_pots_time[i] = sum(E_pots[:i + 1]) / (i + 1)
        virial_time[i] = sum(virials[:i + 1]) / (i + 1)
    plt.figure()
    plt.plot(np.arange(Nt + 1) * dt, E_kins_time / float(N), label='Potential energy')
    plt.plot(np.arange(Nt + 1) * dt, E_pots_time / float(N), label='Kinetic energy')
    plt.plot(np.arange(Nt + 1) * dt, (E_kins_time + E_pots_time) / float(N), label='Total energy')
    plt.xlabel('time [s]')
    plt.ylabel('energy')
    plt.legend()
    plt.savefig('fig_4.jpg')


    T = E_kins_time[-1] / float(N)
    P = E_kins_time[-1] / V + virial_time[-1] / (2 * V)

    print 'temperature', T
    print 'pressure', P
    density = N / V
    print 'ideal pressure', density * T
    print 'press - measured', P - density * T, virial_time[-1] / (2 * V)


def problem_5():
    global flag
    flag = False
    r = np.array(tuple(product(np.linspace(0, 6, num=7)[:-1], repeat=2))) + .5
    v = np.zeros(r.shape)

    N, dim = r.shape
    Nt = 10000
    dt = 0.01
    Ls = [6., 6.]
    x, y, v, E_pots, E_kins, virials, v_cm, r, r_old = molecular_dyn(r, v, Nt, dt, Ls)
    plt.figure()
    plt.xlim(0, 6)
    for i in range(3):
        plt.plot(x[i, :], y[i, :], label='Particle: ' + str(i + 1), marker='')

    plt.legend()
    plt.xlabel('x [m]')
    plt.ylabel('y [m]')
    plt.grid(linestyle='--')
    plt.savefig('0_v_grid_2particles.jpg')

    plt.figure()
    plt.plot(r[:, 0], r[:, 1], '.', label='time = ' + str(Nt * 0.01) + ' s')
    plt.plot(r_old[:, 0], r_old[:, 1], '.', label='time = ' + str(100 * 0.01) + ' s')
    r = np.array(tuple(product(np.linspace(0, 6, num=7)[:-1], repeat=2))) + .5
    plt.plot(r[:, 0], r[:, 1], '.', label='time = ' + str(0 * 0.01) + ' s')

    # r = np.array(tuple(product(np.linspace(0, 6, num=7)[:-1], repeat=2))) + .5
    # v = np.zeros(r.shape)
    # flag = True
    # print 'second'
    # x, y, v, E_pots, E_kins, virials, v_cm = molecular_dyn(r, v, 100, dt, Ls)
    # plt.plot(x[:, -1], y[:, -1], '.', label='time = ' + str(100 * 0.01) + ' s')
    # plt.plot(x[:, 0], y[:, 0], '.', label='time = ' + str(0 * 0.01) + ' s')
    # plt.plot()
    plt.legend()
    plt.xlabel('x [m]')
    plt.ylabel('y [m]')
    plt.grid(linestyle='--')
    plt.savefig('0_v_grid_all.jpg')

    r = np.array(tuple(product(np.linspace(0, 6, num=7)[:-1], repeat=2))) + .5

    theta = np.random.random((36, 1)) * 2 * np.pi
    v = 2 * np.hstack((np.cos(theta), np.sin(theta)))
    v -= np.sum(v, axis=0) / 36.

    N, dim = r.shape
    Nt = 10000
    dt = 0.01
    Ls = [6., 6.]
    flag = False
    x, y, v, E_pots, E_kins, virials, v_cm, r, r_old = molecular_dyn(r, v, Nt, dt, Ls)
    plt.figure()
    plt.xlim(0, 6)
    for i in range(3):
        a = np.abs(np.diff(y[i, :])) >= 0.5
        b = np.abs(np.diff(x[i, :])) >= 0.5
        pos = np.where(np.logical_or(a, b))[0]
        x[i, pos] = np.nan
        y[i, pos] = np.nan
        plt.plot(x[i, :], y[i, :], label='Particle: ' + str(i + 1), marker='')

    plt.legend()
    plt.xlabel('x [m]')
    plt.ylabel('y [m]')
    plt.grid(linestyle='--')
    plt.savefig('2_v_grid_2particles.jpg')


if __name__ == '__main__':
    global interaction
    global flag
    flag = True
    plt.rc('text', usetex=True)
    print 'Problem 1'
    problem_1()
    interaction = True
    print 'Problem 2'
    problem_2()
    print 'Problem 3'
    problem_3()
    print 'Problem 4'
    problem_4()
    print 'Problem 5'
    problem_5()

