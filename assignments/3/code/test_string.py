import numpy as np
import matplotlib.pyplot as plt


def initialize_string_wave(dimensions, init_cond):
    u = np.zeros(dimensions)
    u[0, :] = init_cond[0]
    u[:, 0] = init_cond[1]
    u[:, -1] = init_cond[2]
    return u


def solve_1d_string(fun, derivate, velocity, steps):
    compute_1st_time_step(fun, derivate, velocity, steps)
    ht, hx = steps
    k1 = ((ht * velocity) / hx) ** 2
    k2 = 2 * (1 - k1)
    for tj in range(1, fun.shape[0] - 1):
        fun[tj + 1, 1:-1] = k1 * (fun[tj, :-2] + fun[tj, 2:]) + \
            k2 * fun[tj, 1:-1] - fun[tj - 1, 1:-1]


def compute_1st_time_step(fun, derivate, velocity, steps):
    ht, hx = steps
    k1 = ((ht * velocity) / hx) ** 2 / 2.
    k2 = (1 - k1 * 2)
    fun[1, 1:-1] = k1 * (fun[0, :-2] + fun[0, 2:]) + \
        k2 * fun[0, 1:-1] + ht * derivate


n = 2
dim = (500, 500)
velocity = (0.25) ** .5

u_t0 = 2.5 * np.sin(n * np.pi * np.linspace(0, 1, dim[0]))
cond = (u_t0, 0, 0)
step = np.linspace(0, 1, dim[0])[1]
wave = initialize_string_wave(dim, cond)
solve_1d_string(wave, 0, velocity, (step, step))

plt.figure()
for i in range(500):
    plt.plot(np.linspace(0, 1, dim[0]), wave[i, :])
plt.show()