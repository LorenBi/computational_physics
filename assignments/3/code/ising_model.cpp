#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm> 
#include <tuple>
#include <cmath>
#include <random>
#include <string>
#include <sstream>


using namespace std;

void calculate_E_M(vector<vector<double>>& S, double * E, double * M);
void metro_step(vector<vector<double>>& S, double T, double * E, double * M);
double calculate_energy_change(vector<vector<double>>& S, unsigned int i, unsigned int j);


void initialize_to_1(vector<vector<double>>& S){
	vector<double> possible_spins = {1, -1};
	for (unsigned int i = 0; i < S.size(); ++i)
	{
		for (unsigned int j = 0; j < S[i].size(); ++j)
		{
			S[i][j] = 1;
		}
	}
}

void print_vector(vector<vector<double>>& S){
	for (unsigned int i = 0; i < S.size(); ++i)
	{
		for (unsigned int j = 0; j < S[i].size(); ++j)
		{

			cout << S[i][j] << " ";
		}
		cout << endl;
	}
}


string ising_montecarlo(vector<vector<double>>& S, int n0, int ns, double T){
	double sumE = 0;
	double sumE2 = 0;
	double sumM = 0;
	double sumM2 = 0;
	double curr_E = 0, curr_M = 0;
	double na, E, E2, M, M2, C, chi;
	double N_s = (double) S.size() * S[0].size();

	stringstream ss;

	calculate_E_M(S, &curr_E, &curr_M);

	for (int i = 0; i < ns; ++i)
	{
		metro_step(S, T, &curr_E, &curr_M);
		if (i >= n0)
		{	

			sumE += curr_E;
			sumE2 += (curr_E * curr_E);
			sumM += abs(curr_M);
			sumM2 += (curr_M * curr_M);
		}
	}

	na = (double) ns - n0;
	E = sumE / na;
	E2 = sumE2 / na;
	M = sumM / na;
	M2 = sumM2 / na;
	C = (E2 - E * E) / (T * T);
	chi = (M2 - M * M) / T;
	ss << E / N_s << ", " << M / N_s << ", " << C / N_s << ", " << chi / N_s << ", " << T;
	return ss.str();
	
}

void metro_step(vector<vector<double>>& S, double T, double * E, double * M){
    double dE;
	for (int i = 0; i < S.size(); ++i){
		for (int j = 0; j < S[i].size(); ++j){
			dE = calculate_energy_change(S, i, j);
			if (exp(- dE / T) > ((double) rand() / (RAND_MAX))){ // 
				S[i][j] = - S[i][j];
				*E += dE;
				*M += 2 * S[i][j];
			}
		}
	}
}


double calculate_energy_change(vector<vector<double>>& S, unsigned int i, unsigned int j){
	double dE;
    double s = S[i][j];
    if (i > 0){
        dE += 2 * s * S[i - 1][j];
    }
    if (i < S.size() - 1){
        dE += 2 * s * S[i + 1][j];
    }
    if (j > 0){
        dE += 2 * s * S[i][j - 1];
    }
    if (j < S[i].size() - 1){
        dE += 2 * s * S[i][j + 1];
    }
    return dE;
}



void calculate_E_M(vector<vector<double>>& S, double * E, double * M){
	for (unsigned int i = 0; i < S.size(); ++i){for (auto& n : S[i]) *M += n;}

	for (unsigned int i = 0; i < S.size(); ++i)
	{
		for (unsigned int j = 0; j < S[i].size(); ++j)
		{
			if (i + 1 != S.size())
			{
				*E += - S[i][j] * S[i + 1][j];
			}
			if (j + 1 != S.size())
			{
				*E += - S[i][j] * S[i][j + 1];
			}
		}
	}
}


int main()
{
	srand ( time(NULL) );

	
    int N[] = {6, 8, 16, 40};
    string file[] = {"ising_6.txt", "ising_8.txt", "ising_16.txt", "ising_40.txt"};
    string file_s[] = {"02.txt", "22.txt", "50.txt"};
    double E, M, dT = 0.2;
    string str;
    stringstream ss;
    
    
    for (int i = 0; i < 4; ++i)
    {
    	vector<vector<double>> S(N[i], vector<double>(N[i]));
    	initialize_to_1(S);

    	ofstream outputFile;

    	outputFile.open(file[i]);
    	cout << file[i] << endl;
  		for (int Tj = 1; Tj < 26; ++Tj)
  		{
	    	E=0; M=0;
			str = ising_montecarlo(S, 1000, 10000, Tj * dT);
			outputFile << str << endl;

			if(N[i] == 40 && (Tj == 9 || Tj == 11 || Tj == 25)){
					for (unsigned int i = 0; i < S.size(); ++i)
					{
						for (unsigned int j = 0; j < S[i].size(); ++j)
						{

							ss << S[i][j] << " ";
						}
						ss << "\n";
					}
				ss << "\n";
			}

		}
    }

    ofstream outputFile;
    outputFile.open("spins.txt");
    outputFile << ss.str();

	return 0;
	
}