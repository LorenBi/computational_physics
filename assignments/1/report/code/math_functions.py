'''
This is the code containing the the mathematical functions used for the
assignment
'''

import numpy as np


# cotangent
def cot(x):
    return np.cos(x) / np.sin(x)


#The rooted equation in the finite well calculation
def rooted_equation(x, Rsquared=36):
    return np.sqrt(Rsquared - x ** 2)


# x*tan(x)
def simmetric_state(x):
    return x * np.tan(x)


# - x * cot(x)
def antisimmetric_state(x):
    if np.sin(x) == 0 and - x * (np.cos(x)) > 0:
        return np.inf
    elif np.sin(x) == 0 and - x * (np.cos(x)) < 0:
        return - np.inf
    return - x * (np.cos(x) / np.sin(x))


# sqrt(r**2 - x**2) - x*tan(x)
def simmetric_constraint(x, Rsquared=36):
    return rooted_equation(x, Rsquared) - simmetric_state(x)


# sqrt(r**2 - x**2) + x*cot(x)
def antisimmetric_constraint(x, Rsquared=36):
    return rooted_equation(x, Rsquared) - antisimmetric_state(x)


# derivative of sqrt(r**2 - x**2) - x*tan(x)
def d_simmetric_costraint(x, Rsquared=36):
    return - x / np.sqrt(36 - x ** 2) - np.tan(x) - x / (np.cos(x) ** 2)


# derivative of sqrt(r**2 - x**2) + x*cot(x)
def d_antisimmetric_constraint(x, Rsquared=36):
    return - x / np.sqrt(36 - x ** 2) + cot(x) - x / (np.sin(x) ** 2)


# simm_number stands for simmetric_costraint with the given number
# I admit it is not good looking but I found it difficult to pass in
# find_zero a function with a given parameter. If this were possible they
# would be needed just the previous for function.
def simm_10(x):
    return simmetric_constraint(x, Rsquared=10)


def simm_100(x):
    return simmetric_constraint(x, Rsquared=100)


def simm_1000(x):
    return simmetric_constraint(x, Rsquared=1000)


def simm_10000(x):
    return simmetric_constraint(x, Rsquared=10000)


def simm_100000(x):
    return simmetric_constraint(x, Rsquared=100000)


def simm_1000000(x):
    return simmetric_constraint(x, Rsquared=1000000)


def simm_1(x):
    return simmetric_constraint(x, Rsquared=1)


def simm_40(x):
    return simmetric_constraint(x, Rsquared=40)


def simm_60(x):
    return simmetric_constraint(x, Rsquared=60)


def simm_5(x):
    return simmetric_constraint(x, Rsquared=5)


def simm_20(x):
    return simmetric_constraint(x, Rsquared=20)


def simm_80(x):
    return simmetric_constraint(x, Rsquared=80)

# just the function used to calculate the normalization
# radial stands for radial part of the wave function
# angular for the angular part.
# n and l specify the quantic numbers
def radial_n2_l1(x):
    return float(1) / 24 * x ** 4 * np.exp(-x)


def radial_n3_l1(x):
    return (1. / 6.) ** 3 * ((2. * x) / 3.) ** 4 * (4 - (2. * x) / 3.) ** 2 * np.exp(-(2. * x) / 3.)


def radial_n3_l2(x):
    return (1. / (5. * 6. ** 3)) * ((2. * x) / 3.) ** 6 * np.exp(-(2. * x) / 3.)


def polar_n2o3_l1(x):
    return (3. * x ** 2) / (4. * np.pi)


def polar_n3_l2(x):
    return (5. / (16. * np.pi)) * (3 * x ** 2 - 1) ** 2


# Used in the interals for the A_33 and F 
def radial_n2_l1_int(x):
    return x ** 2 * radial_n2_l1(x)


def radial_n3_l1_int(x):
    return x ** 2 * radial_n3_l1(x)


def radial_n3_l2_int(x):
    return x ** 2 * radial_n3_l2(x)


def polar_n3_l2_int(x):
    return 2 * np.pi * (3 * x ** 2 - 1) * polar_n3_l2(x)


def polar_n2o3_l1_int(x):
    return 2 * np.pi * (3 * x ** 2 - 1) * polar_n2o3_l1(x)
