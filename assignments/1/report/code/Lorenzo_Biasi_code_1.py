'''
The code used to give the output.
This is the coded that it is needed to run for having the
solutions of the assignment
'''

import matplotlib.pyplot as plt
import numpy as np
import pylab as pl
from solve_zero import find_zero, Newton_Raphson
from math_functions import antisimmetric_constraint, d_antisimmetric_constraint
from math_functions import simmetric_constraint, d_simmetric_costraint, cot
import math_functions as mf
import interpol as LB1
from gauss_integrate import *


# This function finds the zeros of the finite well with a radius of 36
# It uses the 4 method requested in the assignment and it prints the solutions
def find_solution_finite_well():
    epsilon = 10 ** -6
    delta = 10 ** -6
    # The intervals are chosen by looking at the plot
    intervals = ((1, 1.4), (3.8, 4.06), (2, 2.71), (5, 5.3))
    parities = ('s', 's', 'a', 'a')
    # first I run through the BI RF SC methods that use the same function
    for method in ['BI', 'RF', 'SC']:
        print method
        for interval, parity in zip(intervals, parities):
            if parity == 's':
                alpha, del_f, Delta_alpha, iterations = find_zero(
                    simmetric_constraint, interval, epsilon, delta=delta, method=method)
            elif parity == 'a':
                alpha, del_f, Delta_alpha, iterations = find_zero(
                    antisimmetric_constraint, interval, epsilon, delta=delta, method=method)
            print align_solution(alpha, del_f, Delta_alpha, iterations, parity)
        print'\n'
    # Then the Newton Raphson
    print 'NR'
    for interval, parity in zip(intervals, parities):
        if parity == 's':
            alpha, del_f, Delta_alpha, iterations = Newton_Raphson(
                simmetric_constraint, d_simmetric_costraint, interval[1], epsilon, delta=delta)
        elif parity == 'a':
            alpha, del_f, Delta_alpha, iterations = Newton_Raphson(
                antisimmetric_constraint, d_antisimmetric_constraint, interval[1], epsilon, delta=delta)
        print align_solution(alpha, del_f, Delta_alpha, iterations, parity)


# It serves to give the output in an easier form to format
def align_solution(alpha, del_f, Delta_alpha, iterations, parity):
    return alpha, alpha ** 2, parity, del_f, Delta_alpha, iterations


# It plots th stationary conditions for the finite well with R = 36
def plot_stationary():
    alpha = pl.frange(0, 6, 0.01)
    alpha_1 = pl.frange(0, 1.49, 0.01)
    alpha_2 = pl.frange(1.6, 4.6, 0.01)
    alpha_3 = pl.frange(4.8, 7, 0.01)
    alpha_4 = pl.frange(0.1, 3, 0.01)
    alpha_5 = pl.frange(3.2, 6.1, 0.01)

    rooted_eq = np.sqrt(36 - alpha ** 2)
    tan_eq_1 = alpha_1 * np.tan(alpha_1)
    tan_eq_2 = alpha_2 * np.tan(alpha_2)
    tan_eq_3 = alpha_3 * np.tan(alpha_3)
    cot_eq_1 = - alpha_4 * cot(alpha_4)
    cot_eq_2 = - alpha_5 * cot(alpha_5)

    plt.rc('text', usetex=True)
    plt.figure('Stationary conditions')
    plt.grid(True)
    plt.ylim([0, 6.5])
    plt.xlim([0, 7])
    plt.xlabel(r'\textbf{\alpha}')
    plt.ylabel('y')
    plt.title('Stationary conditions')
    plt.plot(alpha, rooted_eq)
    plt.plot(alpha_1, tan_eq_1, color='red')
    plt.plot(alpha_4, cot_eq_1, color='green')
    plt.plot(alpha_2, tan_eq_2, color='red')
    plt.plot(alpha_3, tan_eq_3, color='red')
    plt.plot(alpha_5, cot_eq_2, color='green')
    plt.legend([r'\sqrt{p^2 +\alpha^2}', 'Simmetric solution',
                'Antisimmetric solution'])


# It calculates the zeros of the fondamental states for the well of
# radius 10, 100, 1000 ecc. And then plots the values in a logarithmic plot
def ten_to_the_stat_states():
    parity = 'S'
    de = 10 ** -6
    functions = [mf.simm_10, mf.simm_100, mf.simm_1000,
                 mf.simm_10000, mf.simm_100000, mf.simm_1000000]
    Es = []
    for fun in functions:
        alpha, del_f, Delta_alpha, iterations = find_zero(
            fun, (1, 1.57), de, delta=de, method='BI')
        print align_solution(alpha, del_f, Delta_alpha, iterations, parity)
        Es.append(alpha ** 2)

    plt.figure('energy')
    plt.grid(True)
    plt.plot(np.array(range(1, 7)), Es, '.')
    plt.plot([0, 6.5], [np.pi ** 2 / 4, np.pi ** 2 / 4], color='red')
    plt.rc('text', usetex=True)
    plt.ylim([0.5, 2.6])
    plt.xlim([0.5, 6.5])
    plt.xlabel(r'\text{log}_{10}{\frac{V_0 2 m a^2}{\hbar^2}}')
    plt.ylabel(r'\textit{2 m a^2 E_0} /\hbar^2')
    plt.legend(['Calculate states', 'Energy of inf well'], loc=4, numpoints=1)


# It calculates the zeros of R = 1, 5, 20 ecc. and than it interpolates the
# data and it gives an approx of the R = 36
def interpolation_stat_states():
    de = 10 ** -6
    functions = [mf.simm_1, mf.simm_5, mf.simm_20, mf.simm_40, mf.simm_60, mf.simm_80]
    intervals = [(0, 0.8), (1, 1.57), (1, 1.57), (1, 1.57), (1, 1.57), (1, 1.57)]
    Es = []
    for fun, interval in zip(functions, intervals):
        alpha, del_f, Delta_alpha, iterations = find_zero(
            fun, interval, de, delta=de, method='BI')
        Es.append(alpha ** 2)

    plt.figure('energy_2')
    plt.grid(True)
    xs = pl.frange(1, 80, 0.01)
    points = np.array([1, 5, 20, 40, 60, 80])
    print '\n coefficient interpolation'
    print LB1.Newtoncoefficient(Es, points)
    print'\n'
    y = [LB1.poli_Newton(Es, points, point) for point in xs]
    plt.plot(points, Es, '.', color='green')
    plt.plot(xs, y)

    plt.rc('text', usetex=True)
    plt.xlabel(r'$\frac{V_0 2 m a^2}{\hbar^2}$')
    plt.ylabel(r'$\frac{2 m a^2 E_0}{\hbar^2}$')

    plt.plot(36, LB1.poli_Newton(Es, points, 36), '*', color='red', markersize=10)
    plt.legend(['Calculated states', 'Interpolation', r'$\frac{V_0 2 m a^2}{\hbar^2}= 36$'], numpoints=1, loc=4)    
    print 'Interpol point for 36:', LB1.poli_Newton(Es, points, 36)
    print  (1.80835537403 - LB1.poli_Newton(Es, points, 36)) / 1.80835537403


# It calculates the normalization of the wave functions given in the second
# part. It calculate 1- integral, to be precise, this is because python
# otherwise rounds to 1.
def calculate_normalization():
    g_lag_points_w = read_get_gauss('gauss_laguerre.txt')
    print 'f(r)'
    print 'n = 2, l = 1', 1 - gauss_integral(mf.radial_n2_l1, g_lag_points_w)
    print 'n = 3, l = 1', 1 - gauss_integral(mf.radial_n3_l1, g_lag_points_w)
    print 'n = 3, l = 2', 1 - gauss_integral(mf.radial_n3_l2, g_lag_points_w)
    print '\n'
    print 'g(eta)'
    g_leg_points_w = read_get_gauss('gauss_100_pontos.dat')
    print 'n = 2, l = 1', 1 - gauss_integral(mf.polar_n2o3_l1, g_leg_points_w) * 2 * np.pi
    print 'n = 3, l = 1', 1 - gauss_integral(mf.polar_n2o3_l1, g_leg_points_w) * 2 * np.pi
    print 'n = 3, l = 2', 1 - gauss_integral(mf.polar_n3_l2, g_leg_points_w) * 2 * np.pi


# It plots the wave-function of hydrogen
def plot_hydrogen():
    radial_fun = [mf.radial_n2_l1, mf.radial_n3_l1, mf.radial_n3_l2]
    plt.figure('radial')
    for radial in radial_fun:
        x = pl.frange(0, 30, 0.01)
        plt.plot(x, radial(x))
    plt.rc('text', usetex=True)
    plt.grid(True)
    plt.xlabel(r'$\frac{r}{a_0}$')
    plt.ylabel(r'$a_0f(\frac{r}{a_0})(\frac{r}{a_0})^2$')
    plt.legend([r'$f_{21}$', r'$f_{31}$', r'$f_{32}$'])

    theta = pl.frange(0, np.pi, 0.01)
    cosine = np.cos(theta)
    sine = np.sin(theta)
    plt.figure('first_angular')
    plt.plot((sine * mf.polar_n2o3_l1(cosine)) / (2 * np.pi), (cosine * mf.polar_n2o3_l1(cosine)) / (2 * np.pi))
    plt.xlabel('x')
    plt.ylabel('z')
    plt.figure('second angular')
    plt.plot((sine * mf.polar_n3_l2(cosine)) / (2 * np.pi), (cosine * mf.polar_n3_l2(cosine)) / (2 * np.pi))
    plt.xlabel('x')
    plt.ylabel('z')


# It calculates F, A_33 and its product
def calculate_integrals():
    # radial_fun = [mf.radial_n2_l1, mf.radial_n3_l1, mf.radial_n3_l2]
    g_lag_points_w = read_get_gauss('gauss_laguerre.txt')
    print 'F'
    print 'n = 2, l = 1', gauss_integral(mf.radial_n2_l1_int, g_lag_points_w)
    print 'n = 3, l = 1', gauss_integral(mf.radial_n3_l1_int, g_lag_points_w)
    print 'n = 3, l = 2', gauss_integral(mf.radial_n3_l2_int, g_lag_points_w)
    g_leg_points_w = read_get_gauss('gauss_100_pontos.dat')
    print 'A_33'
    print 'n = 2, l = 1', gauss_integral(mf.polar_n2o3_l1_int, g_leg_points_w)
    print 'n = 3, l = 1', gauss_integral(mf.polar_n2o3_l1_int, g_leg_points_w)
    print 'n = 3, l = 2', gauss_integral(mf.polar_n3_l2_int, g_leg_points_w)
    print 'F A_33'
    print 'n = 2, l = 1', gauss_integral(mf.polar_n2o3_l1_int, g_leg_points_w) * gauss_integral(mf.radial_n2_l1_int, g_lag_points_w)
    print 'n = 3, l = 1', gauss_integral(mf.polar_n2o3_l1_int, g_leg_points_w) * gauss_integral(mf.radial_n3_l1_int, g_lag_points_w)
    print 'n = 3, l = 2', gauss_integral(mf.polar_n3_l2_int, g_leg_points_w) * gauss_integral(mf.radial_n3_l2_int, g_lag_points_w)


plot_stationary()
find_solution_finite_well()
ten_to_the_stat_states()
interpolation_stat_states()


calculate_normalization()
plot_hydrogen()
calculate_integrals()
plt.show()