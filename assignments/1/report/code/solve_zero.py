'''
The code used to find the zeros of a function
'''

import numpy as np


def find_zero(f, interval, epsilon, method='BI', delta=np.inf, NITMAX=10 ** 6):
    # Form here
    close_to_zero = abs(f(interval[1])) < epsilon
    points_close = abs(interval[1] - interval[0]) < delta
    if close_to_zero and points_close:
        return interval[1], f(interval[1]), abs(interval[1] - interval[0]), 1
    close_to_zero = abs(f(interval[0])) < epsilon
    if close_to_zero and points_close:
        return interval[0], f(interval[0]), abs(interval[1] - interval[0]), 1
    # to here is just to avoid problems given by the initial conditions
    for i in range(1, NITMAX):
        interval = find_next_interval(f, interval, method)
        close_to_zero = abs(f(interval[1])) < epsilon
        points_close = abs(interval[1] - interval[0]) < delta
        if close_to_zero and points_close:
            return interval[1], f(interval[1]), abs(interval[1] - interval[0]), i


def Newton_Raphson(f, df, x, epsilon, delta=np.inf, NITMAX=10 ** 6):
    if abs(f(x)) < epsilon and delta == np.inf:
        return x, f(x), None, 1
    for i in range(1, NITMAX):
        old_x = x
        x = x - f(x) / df(x)
        if abs(f(x)) < epsilon and abs(old_x - x) < delta:
            return x, f(x), abs(old_x - x), i


def find_next_interval(f, interval, method):
    if method == 'BI':
        return bisection_interval(f, interval)
    elif method == 'RF':
        return regula_falsi(f, interval)
    elif method == 'SC':
        return secante(f, interval)


def bisection_interval(f, interval):
    x_next = float(interval[1] + interval[0]) / 2
    if f(x_next) * f(interval[1]) <= 0:
        return (interval[1], x_next)
    else:
        return (interval[0], x_next)


def regula_falsi(f, interval):
    x_n = interval[0]
    x_n1 = interval[1]
    x_next = (x_n * f(x_n1) - x_n1 * f(x_n)) / (f(x_n1) - f(x_n))
    if f(x_next) * f(x_n1) <= 0:
        return (x_next, interval[1])
    else:
        return (interval[0], x_next)


def secante(f, interval):
    x_n = interval[0]
    x_n1 = interval[1]
    x_next = (x_n * f(x_n1) - x_n1 * f(x_n)) / (f(x_n1) - f(x_n))
    return (x_n1, x_next)
