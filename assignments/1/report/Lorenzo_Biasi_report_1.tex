\documentclass{article}

\usepackage{fancyhdr}
\usepackage{extramarks}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{tikz}
\usepackage[plain]{algorithm}
\usepackage{algpseudocode}
\usepackage{listings}

\DeclareFixedFont{\ttb}{T1}{txtt}{bx}{n}{9} % for bold
\DeclareFixedFont{\ttm}{T1}{txtt}{m}{n}{9}  % for normal
% Defining colors
\usepackage{color}
\definecolor{deepblue}{rgb}{0,0,0.5}
\definecolor{deepred}{rgb}{0.6,0,0}
\definecolor{deepgreen}{rgb}{0,0.5,0}


\usepackage{listings}

% Python style for highlighting
\lstset{
language=Python,
backgroundcolor=\color{white},
basicstyle=\ttm,
otherkeywords={self},            
keywordstyle=\ttb\color{deepblue},
emph={MyClass,__init__},          
emphstyle=\ttb\color{deepred},    
stringstyle=\color{deepgreen},
commentstyle=\color{red},
frame=tb,                         
showstringspaces=false            
}

\usetikzlibrary{automata,positioning}

%
% Basic Document Settings
%

\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\linespread{1.1}

\pagestyle{fancy}
\lhead{\hmwkAuthorName}
\chead{\hmwkClass\ (\hmwkClassInstructor): \hmwkTitle}
\rhead{\firstxmark}
\lfoot{\lastxmark}
\cfoot{\thepage}

\renewcommand\headrulewidth{0.4pt}
\renewcommand\footrulewidth{0.4pt}

\setlength\parindent{0pt}

%
% Create Problem Sections
%

\newcommand{\enterProblemHeader}[1]{
    \nobreak\extramarks{}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
}

\newcommand{\exitProblemHeader}[1]{
    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
    \stepcounter{#1}
    \nobreak\extramarks{Problem \arabic{#1}}{}\nobreak{}
}

\setcounter{secnumdepth}{0}
\newcounter{partCounter}
\newcounter{homeworkProblemCounter}
\setcounter{homeworkProblemCounter}{1}
\nobreak\extramarks{Problem \arabic{homeworkProblemCounter}}{}\nobreak{}

%
% Homework Problem Environment
%
% This environment takes an optional argument. When given, it will adjust the
% problem counter. This is useful for when the problems given for your
% assignment aren't sequential. See the last 3 problems of this template for an
% example.
%
\newenvironment{homeworkProblem}[1][-1]{
    \ifnum#1>0
        \setcounter{homeworkProblemCounter}{#1}
    \fi
    \section{Problem \arabic{homeworkProblemCounter}}
    \setcounter{partCounter}{1}
    \enterProblemHeader{homeworkProblemCounter}
}{
    \exitProblemHeader{homeworkProblemCounter}
}

%
% Homework Details
%   - Title
%   - Due date
%   - Class
%   - Section/Time
%   - Instructor
%   - Author
%

\newcommand{\hmwkTitle}{Homework\ \#1}
\newcommand{\hmwkDueDate}{March 7, 2017}
\newcommand{\hmwkClass}{Computational Physics}
\newcommand{\hmwkClassInstructor}{Professor Isaac Vidana}
\newcommand{\hmwkAuthorName}{\textbf{Lorenzo Biasi}}

%
% Title Page
%

\title{
    \vspace{2in}
    \textmd{\textbf{\hmwkClass:\ \hmwkTitle}}\\
    \normalsize\vspace{0.1in}\small{Due\ on\ \hmwkDueDate\ at 3:10pm}\\
    \vspace{0.1in}\large{\textit{\hmwkClassInstructor}}
    \vspace{3in}
}

\author{\hmwkAuthorName}
\date{}

\renewcommand{\part}[1]{\textbf{\large Part \Alph{partCounter}}\stepcounter{partCounter}\\}

%
% Various Helper Commands
%

% Useful for algorithms
\newcommand{\alg}[1]{\textsc{\bfseries \footnotesize #1}}

% For derivatives
\newcommand{\deriv}[1]{\frac{\mathrm{d}}{\mathrm{d}x} (#1)}

% For partial derivatives
\newcommand{\pderiv}[2]{\frac{\partial}{\partial #1} (#2)}

% Integral dx
\newcommand{\dx}{\mathrm{d}x}

% Alias for the Solution section header
\newcommand{\solution}{\textbf{\large Solution}}

% Probability commands: Expectation, Variance, Covariance, Bias
\newcommand{\E}{\mathrm{E}}
\newcommand{\Var}{\mathrm{Var}}
\newcommand{\Cov}{\mathrm{Cov}}
\newcommand{\Bias}{\mathrm{Bias}}

\begin{document}

\maketitle

\pagebreak
\underline{\large{\textbf{Part I: Finite square well}}}\\

In this part of the project we are going to find the energy of all the bound
states of the quantum finite square well potential shown in the figure

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h]
\begin{center}
\includegraphics[width=8.0cm]{fig1.eps}
\end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The most general solutions of the Schrödinger equation
\vskip0.2cm
\begin{equation} 
\Big(-\frac{\hbar^2}{2m}\frac{d^2}{dx^2}+V(x)\Big)\Psi(x)=E\Psi(x)
\,\,\,\,\,\, \mbox{com} \,\,\,\,\,\,
V(x) = \left\{
\begin{array}{cc}
V_0 &  \mbox{se} \,\,\, |x| > a \\
0 & \mbox{se} \,\,\, |x| < a \\
\end{array}
\right.
\nonumber
\nonumber \\ 
\end{equation}
\vskip0.2cm
with energy $E < V_0$ are of type:
\begin{eqnarray}
\Psi_{I}(x) &=& Ae^{k_1x} \phantom{aaaaaaaaa}\,\,\,\,\, \,\mbox{se} \,\,\,  -\infty < x < -a \nonumber \\
\Psi_{II}(x) &=& Be^{ik_2x}+Ce^{-ik_2x} \,\,\,\,\,\, \mbox{se} \,\,\,\,\,   -a  \leq x \leq a\nonumber \\
\Psi_{III}(x) &=& De^{-k_1x} \phantom{aaaaaaaa}\,\,\,\,\, \,\,\mbox{se} \,\,\,\,\,\,\,\,\,\,\,  a < x < \infty \nonumber
\end{eqnarray}
where
\begin{equation}
k_1=\sqrt{\frac{2m}{\hbar^2}(V_0-E)} \,\,\,\,\,\, \mbox{e} \,\,\,\,\,\, k_2=\sqrt{\frac{2m}{\hbar^2}E} \nonumber
\end{equation}

%%%%


\begin{homeworkProblem}
Verify that the wave functions $\Psi_I(x), \Psi_{II}(x)$ and $\Psi_{III}(x)$ are solutions of
the Schrodinger equation. \\

\textbf{Solution} \\
For $\Psi_I(x), \Psi_{III}(x)$ the solution is almost the same, because the equation is linear, so we can simplify the costant of normalization, and because we derive twice so that the sign of the exponent is unimportat. The expicit calculation for $\Psi_I(x)$:
$$- \frac{\hbar^2}{2m}\frac{d^2}{dx^2}(Ae^{k_1x}) + V_0 Ae^{k_1x} = E Ae^{k_1x}  \quad  - \frac{\hbar^2}{2m} k_1^2 e^{k_1x} + V_0 e^{k_1x} = E e^{k_1x}  \quad  \frac{\hbar^2}{2m} k_1^2 = E - V_0$$
From this we find that $k_1 = \sqrt{(\frac{2m}{\hbar^2}(V_0 - E))}$. \\
For $\Psi_{II}(x)$ the calculation are similar:
$$ - \frac{\hbar^2}{2m}\frac{d^2}{dx^2}(Be^{i k_2x}) - \frac{\hbar^2}{2m}\frac{d^2}{dx^2}(Ce^{-i k_2x}) = E (Be^{i k_2x} + Ce^{-i k_2x}) $$ $$ - \frac{\hbar^2}{2m} B k_2^2 (-1) e^{i k_2x} - \frac{\hbar^2}{2m} C k_2^2 (-1) e^{-i k_2x} = E (Be^{i k_2x} + Ce^{-i k_2x}) $$ $$ k_2^2 \frac{\hbar^2}{2m} = E$$
And that final equation brings us to $k_2 = \sqrt{\frac{2m}{\hbar}E}$
\end{homeworkProblem}

\begin{homeworkProblem}
Imposing the continuity of the wave function and its first derivative at the points $x = −a$ e $x = a$, show that:
    \begin{enumerate}
    \item The states with positive parity (i.e, $\Psi(-x) = \Psi(x)$)  leads to the transcendental equation
        \begin{equation}
        k_1 = k_2 \text{tan}(k_2 a)
        \end{equation}
    \item The states with negative parity (i.e, $\Psi(-x) = - \Psi(x)$) leads to the transcendental equation
        \begin{equation}
        k_1 = - k_2 \text{cot}(k_2 a)
        \end{equation}
    \end{enumerate}
Defining the adimentional quantitiers
$$\alpha \equiv k_2 a = \sqrt{\frac{2ma^2}{\hbar^2}E} \quad e \quad p \equiv \sqrt{\frac{2ma^2}{\hbar^2}V_0}$$
these equations can be rewritten as
    \begin{equation}
    \sqrt{p^2 - \alpha^2} = \alpha \text{tan}(\alpha) \Leftrightarrow \cos{\alpha}\sqrt{p^2 - \alpha^2} = \alpha \sin{\alpha}
    \end{equation}
    and
    \begin{equation}
    \sqrt{p^2 - \alpha^2} = - \alpha \text{cot}(\alpha) \Leftrightarrow \sin{\alpha}\sqrt{p^2 - \alpha^2} = - \alpha \cos{\alpha}
    \end{equation}
respectively. Note that the solution of equations 1 e 2 (or 3 e 4) allows
to find the energy of the bound states with positive and negative parity,
respectively. However, the solution of these equations is not analytical
and can only be obtained either graphically or numerically. \\

\textbf{Solution} \\
We will do the calculation for (1) and (2) separetely.
    \begin{enumerate}
    \item The fastest way to achive (1) is by imposing the the parity on $\Psi(x)$ and once we have simplified the state function use the continuity and continuity of the derivate to find the conditions on the energy states.\\
    Let's consider the function outside the potential well first. If we take $\xi > a$ we impose:
    $$ \Psi_I( - \xi) = \Psi_{III}(\xi) \Leftrightarrow A e^{- \xi k_1} = D e^{- \xi k_1} $$
    So we get that $A = D = C_I$, we will see that the constant of normalization doesn't play a role in the calculation.\\
    For the state function inside the well we do the same. Let's take $\eta < a$ and impose the parity:
    $$ \Psi_{II}(\eta) = Be^{i k_2\eta} + Ce^{-i k_2\eta}  \quad e \quad \Psi_{II}(- \eta) = Be^{-i k_2\eta} + Ce^{i k_2\eta}$$
    $$ \Psi_{II}(\eta) =  \Psi_{II}(- \eta) = (B + C) \frac{e^{i k_2\eta} + e^{-i k_2\eta}}{2} \Leftrightarrow \Psi_{II}(x) = C_{II} \cos{k_2 x}$$
    Where $C_{II} = (B + C)$. \\
    Now we impose the continuity of the function and its derivative:
    $$
        \begin{cases} 
        C_I e^{-k_1a} = C_{II} \cos{k_2a} \\
        - k_1 C_I e^{-k_1a} = - k_2 C_{II} \sin{k_2a}\\      
        \end{cases}
    $$
    And by dividing the second equation by the first we get the equation (1)\\
    \item We impose the disparity outside the potential well and we have:
    $$  \Psi_I( - \xi) = - \Psi_{III}(\xi) \Leftrightarrow A e^{- \xi k_1} = - D e^{- \xi k_1}$$
    So $- A = D = C_I$.\\
    For outside the potential well:
    $$ \Psi_{II}(\eta) = Be^{i k_2\eta} + Ce^{-i k_2\eta}  \quad e \quad \Psi_{II}(- \eta) = - Be^{-i k_2\eta} + Ce^{i k_2\eta}$$
    $$ \frac{\Psi_{II}(\eta) - \Psi_{II}(- \eta)}{2} = i(B + C) \frac{e^{i k_2\eta} - e^{-i k_2\eta}}{2i} \Leftrightarrow \Psi_{II}(x) = C_{II} \sin{k_2 x}$$
    Where $C_{II} = i(B + C)$. \\
    Now we impose the continuity of the function and its derivative:
    $$
        \begin{cases} 
        C_I e^{-k_1a} = C_{II} \sin{k_2a} \\
        - k_1 C_I e^{-k_1a} = k_2 C_{II} \cos{k_2a}\\      
        \end{cases}
    $$
    And by dividing the second equation by the first we get the equation (2)
    \end{enumerate}
\end{homeworkProblem}
\begin{homeworkProblem}
Solve graphically equations 3 and 4 for the quantity $\alpha$ taking $V_0 = 36 \hbar^2/2ma^2$. That is, make a figure of $\sqrt{p^2 - \alpha^2}$ , $\alpha \tan{\alpha}$ and $-\alpha \cot{\alpha}$as a function of $\alpha$ and identify all the values of $\alpha$ that are solution of equations 3 and 4 for this value $V_0$ . Comment briefly the result obtained.\\
\textbf{Solution} \\
When we use $V_0$ given we easily see that the function $\sqrt{p^2 - \alpha^2}$ becomes a circle of radius of 6. We can plot this result and obtain:
\begin{figure}[H]
\centering
\includegraphics[width=.8\textwidth]{stationary_conditions.png}
\caption{Stationary conditions}
\end{figure}
The points where the circle intersects with the other functions show the conditions for having a stationary states. We hava a discrete number of solutions, as expected because we are looking at bound states. The fondamental one is the symmetric one, also as expected.
\end{homeworkProblem}

\begin{homeworkProblem}
Now solve numerically equations 3 and 4 using the 4 methods explained
in the lessons to solve non-linear equations: Bisection (BI), Regula Falsi
(RF), Newton-Rapson (NR) and Secant (SC)). Take again $V_0 = 36 \hbar^2/2ma^2$ and determine, as in the previous point, all the values of $\alpha$ that are solution of these equations for this particular value of $V_0$. Determine the solutions imposing simultaneously the following two criteria: (i): value of the function at the zero $|f(\alpha)| \leq 10^{-6}$ and (ii) error in the localization of the zero $|\Delta\alpha| \leq 10^{-6}$. For each method write the result in a table of the
following form:
\begin{center}

 \begin{tabular}{||c c c c c c||} 
 \hline
 $\alpha$ & $\text{E}(\hbar^2 / 2ma^2)$ & Parity & $|f(\alpha)|$ & $|\Delta\alpha|$ & Number of iterations\\ [0.5ex] 
 \hline\hline
 ... & ... & ... & ... & ... & ... \\ 
 \hline
  ... & ... & ... & ... & ... & ... \\ 
 \hline
\end{tabular}
\end{center}
Note tha the energy E is given in units of $\hbar^2 / 2ma^2$. Therefore, you do not need to give an particular value to the mass m or to the parameter a.
Identify the fundamental state.\\
\textbf{Solution} \\
For the Bisection (BI) we have:
\begin{center}

 \begin{tabular}{||c c c c c c||} 
 \hline
 $\alpha$ & $\text{E}(\hbar^2 / 2ma^2)$ & Parity & $|f(\alpha)|$ & $|\Delta\alpha|$ & Number of iterations\\ [0.5ex] 
 \hline\hline
 1.34475107192993 & 1.808355445456700 & S & -8.3244635e-07 & 9.53674317e-08 & 22 \\
 \hline
 3.98582614898681 & 15.88681008994706 & S & 6.35424604e-07 & 4.95910644e-07 & 19 \\
 \hline
 2.67878308534622 & 7.175878818337023 & A & 3.68680341e-07 & 1.69277191e-07 & 22 \\
 \hline
 5.22596344947814 & 27.31069397528155 & A & 7.45752199e-07 & 1.43051147e-07 & 21 \\
 \hline
\end{tabular}
\end{center}

For the Regula Falsi (RF) we have:
\begin{center}

 \begin{tabular}{||c c c c c c||} 
 \hline
 $\alpha$ & $\text{E}(\hbar^2 / 2ma^2)$ & Parity & $|f(\alpha)|$ & $|\Delta\alpha|$ & Number of iterations\\ [0.5ex] 
 \hline\hline
 1.34475104537578 & 1.808355374039277 & S & -3.55271367e-15 & 2.22044604e-16 & 26 \\
 \hline
 3.98582620651249 & 15.88681054852175 & S & -8.88178419e-16 & 2.66453525e-15 & 14 \\
 \hline
 2.67878310847244 & 7.175878942237318 & A & -1.77635683e-15 & 2.66453525e-15 & 13 \\
 \hline
 5.22596353031323 & 27.31069482016400 & A & -6.66133814e-15 & 1.77635683e-15 & 12 \\
 \hline
\end{tabular}
\end{center}

For the Secant (SC) we have:
\begin{center}

 \begin{tabular}{||c c c c c c||} 
 \hline
 $\alpha$ & $\text{E}(\hbar^2 / 2ma^2)$ & Parity & $|f(\alpha)|$ & $|\Delta\alpha|$ & Number of iterations\\ [0.5ex] 
 \hline\hline
 1.34475104537187 & 1.808355374028759 & S & 1.225917145e-10 & 4.86115487e-08 & 7 \\
 \hline
 3.98582620651153 & 15.88681054851416 & S & 1.051603248e-11 & 3.55452360e-08 & 5 \\
 \hline
 2.67878310847244 & 7.175878942237321 & A & -9.76996261e-15 & 4.25528057e-10 & 6 \\
 \hline
 5.22596353031323 & 27.31069482016397 & A & 2.620126338e-14 & 1.02598463e-09 & 5 \\
 \hline
\end{tabular}
\end{center}

For the Newton-Rapson (NR) we have:
\begin{center}

 \begin{tabular}{||c c c c c c||} 
 \hline
 $\alpha$ & $\text{E}(\hbar^2 / 2ma^2)$ & Parity & $|f(\alpha)|$ & $|\Delta\alpha|$ & Number of iterations\\ [0.5ex] 
 \hline\hline
 1.34475104537578 & 1.808355374039277 & S & -3.55271367e-15 & 3.02549096e-11 & 5 \\
 \hline
 3.98582620651249 & 15.88681054852175 & S & -8.88178419e-16 & 2.64910227e-09 & 4 \\
 \hline
 2.67878310847244 & 7.175878942237318 & A & -1.77635683e-15 & 1.24055876e-10 & 4 \\
 \hline
 5.22596353031323 & 27.31069482016400 & A & 1.776356839e-15 & 4.79500883e-11 & 4 \\
 \hline

 \hline
\end{tabular}
\end{center}

From the tables shown we can see how the methods are in order of speed: with the slowest being the Bisection method and fastest the Newton-Rapson. This doesn't come cheap, in fact Newton-Rapson can create many problems if the starting point is not well chosen.
\end{homeworkProblem}

\begin{homeworkProblem}
Consider now the following values of the parameter $V_0$ = 10, 100, 1000, 10000, 100000 and 1000000 $\hbar^2/2ma^2$. Use one of the 4 metods to solve non-linear equations and determine for each of these values of $V_0$ the energy of the fundamental state $E_0$ . Find the solution in each case imposing again
simultaneously the two criteria: (i): value of the function at the zero $|f(\alpha)| \leq 10^{-6}$ and (ii) error in the localization of the zero $|\Delta\alpha| \leq 10^{-6}$. For each method write the result in a table of the
following form:
\begin{center}

 \begin{tabular}{||c c c c c c||} 
 \hline
 & $V_0$ & $\alpha$ & $\text{E}(\hbar^2 / 2ma^2)$ $|f(\alpha)|$ & $|\Delta\alpha|$ & Number of iterations\\ [0.5ex] 
 \hline\hline
 ... & ... & ... & ... & ... & ... \\ 
 \hline
  ... & ... & ... & ... & ... & ... \\ 
 \hline
\end{tabular}
\end{center}
\textbf{Solution} \\
The method I chose to find the zero was the bisection method. Even if this method is slow, it is more stable numerically. Also the time it would have taken me to find the right initial condition for the other methods would have been for sure more than the running time of the bisection method.
The values found were:
\begin{center}

 \begin{tabular}{||c c c c c c||} 
 \hline
 $V_0(\hbar^2 / 2ma^2)$ & $\alpha$ & $\text{E}(\hbar^2 / 2ma^2)$ & $|f(\alpha)|$ & $|\Delta\alpha|$ & Number of iterations\\ [0.5ex] 
 \hline\hline
 $10^1$ & 1.18626084089 & 1.4072147826356 & -2.757424741e-07 & 1.358985901e-07 & 22 \\
 \hline
 $10^2$ & 1.42755177736 & 2.0379040770517 &  1.081150298e-07 & 1.358985901e-07 & 22 \\
 \hline
 $10^3$ & 1.52262797683 & 2.3183959558399 &  7.507567367e-07 & 1.698732376e-08 & 25 \\
 \hline
 $10^4$ & 1.55524326712 & 2.4187816199332 & -2.174405153e-07 & 2.654270137e-10 & 31 \\
 \hline
 $10^5$ & 1.56584467094 & 2.4518695335343 & -6.113895665e-07 & 6.635669791e-11 & 33 \\
 \hline
 $10^6$ & 1.56922709905 & 2.4624736884000 & -3.073566858e-07 & 4.147127086e-12 & 37 \\
 \hline
\end{tabular}
\end{center}

We can see how, as the potential get deeper, the energy doesn't change by much. This can be better seen in the plot \ref{energy}

\begin{figure}[H]
\centering
\includegraphics[width=.8\textwidth]{energy.png}
\caption{Stationary energies. The x argument is logarithmic for giving a better view of the points chosen}
\label{energy}
\end{figure}
For $V_0$ the energy should converge to $\pi^2 \hbar^2 / 2m(2a)^2$ so our plot should have an asintote for $\pi^2 / 4 = 2.4674011$. This is because as the well gets deeper, the fondemental stationary state start to resamble the one of an infinite potential well (which can be solved analytically).
\end{homeworkProblem}
\begin{homeworkProblem}
Build the polynomial that interpolates the function $E_0(V_0)$ passing through
the points $V_0$ = 1, 5, 20, 40, 60 and 80 $\hbar^2/2ma^2$. Write the table of the divided diferences. Identify the coefficients of the polynomial. Make a figure showing the set of points $E_0(V_0)$ and the corresponding polynomial. Finally, evauate the polynomial at the point $V_0 = 36 \hbar^2/2ma^2$ and compare
the result for the energy of the fundamental state with that obtained in
the question 4.\\
\textbf{Solution} \\
We used the bisection method for finding the zeros and then with the Newton method we interpolated the data. The result can be seen in the plot \ref{energy_2}.
\begin{figure}[H]
\centering
\includegraphics[width=.8\textwidth]{energy_2.png}
\caption{Stationary energies with the interpolation}
\label{energy_2}
\end{figure}
We chose the method of Newton for this interpolation.\\
The point for a potential $V_0 = 36 \hbar^2/2ma^2$ given by the interpolation is $E_0 = 1.7575782481$, which is about 2.8 $\%$ away the correct value, so depending on the precision needed this method can or cannot be used. Int the following table there are the dividend coefficients and in the first line there are the coefficents that can be used for building the polynomial that interpolates the data.
\begin{table}[H]
\begin{center}
 \begin{tabular}{||c c c c c c||} 
 \hline
  5.46246647e-01  & 1.50312243e-01  &-6.18490903e-03  & 1.41747897e-04& -2.23825305e-06& 2.69186847e-08\\
  1.14749562e+00 &  3.27989717e-02 & -6.56741056e-04  & 9.69096681e-06 &-1.11676962e-07 &0 \\
  1.63948020e+00 &  9.81303476e-03 & -1.23737882e-04  & 1.31519464e-06 &0                &0\\
  1.83574089e+00 &  4.86351950e-03 & -4.48262032e-05  & 0            &  0         &       0\\
  1.93301128e+00  & 3.07047137e-03 &  0                &0  &            0&                0\\
  1.99442071e+00 &  0&                0&                0&              0&                0\\
  \hline
 \end{tabular}
\end{center}
\end{table}

\end{homeworkProblem}\\
\pagebreak\\
\underline{\large{\textbf{Part II: Quadrupolar Moment of the Hidrogen Atom}}}\\
In this part of the project you are to calculate some properties of the charge distribution of an electron in the hidrogen atom (quantum), for different states.\\ The electric potential generated by a given charge distribution which is situated in a given region of the sace can be written as a superposition of different terms. When we are far from the charge, its distribution can be well approximated by that of a point-like charge. However, when we approach the charger, more detail information of the charge distribution is needed to obtain the corresponding electrical potential.
The Taylor expanson of $1 / | \vec{r} - \vec{r}'|$ for $|\vec{r}| \gg |\vec{r}'|$ is
\begin{equation}
\frac{1}{| \vec{r} - \vec{r}'|} = \frac{1}{r} + \displaystyle\sum_{i=1}^{3} \frac{r_i r_i'}{r^3} + \displaystyle\sum_{i=1}^{3}\displaystyle\sum_{j=1}^{3} \frac{r_i r_j}{2r^5}(3r_i'r_j' -r'^2 \delta_{ij}) + ...
\end{equation}
where $r \equiv |\vec{r}|$, $r' \equiv |\vec{r}'|$ are the modulus of the vectors $\vec{r}$ e $\vec{r}'$ , and $\delta_{ij}$ is the Kronecker delta. This equaton allows us to write the potential generated by a charge distribution $ \rho(\vec{r}')$, in a point $vec{r}$ at large distances of the region where $pho \ne 0$, as
\begin{equation}
V(\vec{r}) = \int d\vec{r}' \frac{\rho(\vec{r}')}{| \vec{r} - \vec{r}'|} = \frac{C}{r} + \frac{1}{r^3} \displaystyle\sum_{i=1}^{3} D_i r_i + \frac{1}{2r^5}\displaystyle\sum_{i=1}^{3}\displaystyle\sum_{j=1}^{3}Q_{ij}r_i r_j + ...
\end{equation}

where $C$ is the total charge (or \textit{monopolar moment of the distribution}), $D_i$ are the components of \textit{the dipolar moment of the distribution} and $Q_{ij}$ are the components of the tensor \textit{quadrupole moment of the distribution} defined as: $C \equiv \int d\vec{r} \rho(\vec{r}), ~ D_i \equiv \int d\vec{r} \rho(\vec{r}) r_i$ and 
\begin{equation}
Q_{ij} \equiv \int d\vec{r} \rho(\vec{r}) (3 r_i r_j - |\vec{r}|^2 \delta_{ij})
\end{equation}
Note that, by construction, $Q_{ij}$ is a symmetric tensor $(Q_{ij} = Q_{ji} $). Putting the origin at the center of the charge (analogously to the baricentrum of mass distribution) the dipole moment vanishes ($D_i = 0,~ i = 1,~2,~3$). In this frame, if the charge distribution has spherical symmetry then the quadrupole moment is identically zero ($Q_{ij} = 0, i,~j = 1,~2,~ 3$). The quadrupole moment, therefore, measures \textit{how far a charge distribution is from the spherical one}.

\begin{homeworkProblem}
Using the definition of the quadrupole moment, Eq. (7), show analitycally
that $Q_{11} + Q_{22} + Q_{33} = 0$ for any $\rho(\vec{r})$.\\
\textbf{Solution}\\
We can explicitly write the trace of $Q_{ij}$ using Eq. (7).
$$
Q_{11} + Q_{22} + Q_{33} = \int d\vec{r} \rho(\vec{r}) (3 r_1^2 + 3 r_2^2 + 3 r_3^2 - 3 |\vec{r}|^2)
$$
We get the equality with 0 by remembering that $|\vec{r}|^2 = r_1^2 + r_2^2 + r_3^2$.

\end{homeworkProblem}
\begin{homeworkProblem}

Consider now a distribution with axial symmetry: given in spherical
coordinates as $\rho(\vec{r})=f(r)g(\cos{\theta})$. Assume that the z axis coincides
with the symmetry axis of the distribution and that $\vec{r}=(x,y,z)=r(\sin{\theta}\cos{\varphi},\sin{\theta}\sin{\varphi},\cos{\theta})$, 
where $r\in[0,\infty[$, $\theta\in[0,\pi]$ e $\varphi\in[0,2\pi[$. The volume element is $d^3\vec{r}=r^2\, dr\, \sin{\theta}\, d\theta \, d\varphi$.
Show that the quadrupole moment can be written as product of a radial and an agular part, $Q_{ij}=F A_{ij}$, onde 
\begin{equation}\label{eq:FA}
  F\equiv \int_0^\infty dr\, r^4 f(r), \quad A_{ij}\equiv \int_0^\pi d\theta \sin{\theta}\,g(\cos{\theta})
\int_0^{2\pi} d\varphi\left(3 \frac{x_i}{r}\frac{x_j}{r}-\delta_{ij}\right)~.
\end{equation}
Like $Q_{ij}$, also $A_{ij}$ is also symmetric and its trace is zero.
Make analytically the integral
over the variable $\varphi$ and show that  $A_{12}=A_{13}=A_{23}=0$, $A_{11}=A_{22}$, 
$2 A_{11}+A_{33}=0$ and, making the variable change $\eta=\cos{\theta}$,
\begin{equation}\label{eq:A33}
A_{33}=2\pi \int_{-1}^1 d\eta\, g(\eta)\, (3\eta^2-1).
\end{equation}
To obtain $A_{ij}$, we obly need to compute one of the diagonal components,
such as for instance $A_{33}$.
For $\rho>0$, the sign $A_{33}$ (and of $Q_{33}$) indicates
whether the distributonis prolate (cigar form) or oblate (pancake form).
The first corresponds to $Q_{33}>0$ whereas the second to $Q_{33}<0$.\\

\textbf{Solution}\\
Writing explicitly $Q_{ij}$ in spherical coordinates it can be easily seen that the integral can be factorized as stated before
\begin{align*}
Q_{ij} &= \int_0^\infty \int_0^\pi \int_0^{2\pi} dr d\theta d\varphi r^2 \sin{\theta} f(r)g(\cos{\theta})(3r_i r_j - r^2 \delta_{ij})\\
& = \int_0^\infty dr\, r^4 f(r) \int_0^\pi d\theta \sin{\theta}\,g(\cos{\theta}) \int_0^{2\pi} d\varphi\left(3 \frac{x_i}{r}\frac{x_j}{r}-\delta_{ij}\right)\\
& = F A_{ij}
\end{align*}
We can see also that $A_{12}=A_{13}=A_{23}=0$
$$
A_{12} = \int_0^\pi d\theta \sin{\theta} g(\cos{\theta}) \int_0^{2\pi} d\varphi ~3 \sin^2{\theta} \sin{\varphi}\cos{\varphi} = 3 \int_0^\pi d\theta \sin^3{\theta} g(\cos{\theta}) \int_0^{2\pi} d\varphi \sin{\varphi}\cos{\varphi}
$$
$$
A_{13} =  \int_0^\pi d\theta \sin{\theta} g(\cos{\theta}) \int_0^{2\pi} d\varphi ~3 \sin{\theta} \cos{\theta} \cos{\varphi} = 3 \int_0^\pi d\theta \sin^2{\theta} \cos{\theta} g(\cos{\theta}) \int_0^{2\pi} d\varphi \cos{\varphi}
$$
$$
A_{23} =  \int_0^\pi d\theta \sin{\theta} g(\cos{\theta}) \int_0^{2\pi} d\varphi ~3 \sin{\theta} \cos{\theta} \sin{\varphi} = 3 \int_0^\pi d\theta \sin^2{\theta} \cos{\theta} g(\cos{\theta}) \int_0^{2\pi} d\varphi \sin{\varphi} 
$$
And since $\int_0^{2\pi} d\varphi \sin{\varphi}\cos{\varphi} = \int_0^{2\pi} d\varphi \sin{\varphi} = \int_0^{2\pi} d\varphi \sin{\varphi} = 0$ The equation above are all equal to 0.\\
We now prove that $A_{11} = A_{22}$
\begin{align*}
A_{11} &= \int_0^\pi d\theta \sin{\theta}\,g(\cos{\theta})
\int_0^{2\pi} d\varphi\left(3 \sin^2{\theta} \cos^2{\varphi} - 1\right)\\
&= \int_0^\pi d\theta \sin{\theta}\,g(\cos{\theta})
\left(3\pi \sin^2{\theta}  - 2\pi\right)\\
&= \int_0^\pi d\theta \sin{\theta}\,g(\cos{\theta})
\int_0^{2\pi} d\varphi\left(3 \sin^2{\theta} \sin^2{\varphi} - 1\right)\\
& = \int_0^\pi d\theta \sin{\theta}\,g(\cos{\theta})
\int_0^{2\pi} d\varphi\left(3 \frac{x_2}{r}\frac{x_2}{r}-\delta_{22}\right)\\
& = A_{22}
\end{align*}
We now prove $2 A_{11}+A_{33}=0$. We know from before that:
$$
2A_{11} = 2 \pi \int_0^\pi d\theta \sin{\theta}\,g(\cos{\theta})
\left(3\sin^2{\theta}  - 2\right)\\
$$
We just need to find that $A_{33}$ is negative that value.
\begin{align}\label{eq:A33_sol}
A_{33} & = \int_0^\pi d\theta \sin{\theta}\,g(\cos{\theta})
\int_0^{2\pi} d\varphi\left(3 \cos^2{\theta} - 1\right) \nonumber \\
& = 2 \pi \int_0^\pi d\theta \sin{\theta}\,g(\cos{\theta}) \left(3 \cos^2{\theta} - 1\right)
\end{align}
Now if we add 2 and delete 2 in the parethesis we can write the cosine as a sine.
\begin{align*}
 A_{33} &= 2 \pi \int_0^\pi d\theta \sin{\theta}\,g(\cos{\theta}) \left(-3 \sin^2{\theta} + 2\right)\\
 & = - 2A_{11}
\end{align*}
To show that we can write $A_{33} = 2\pi \int_{-1}^1 d\eta\, g(\eta)\, (3\eta^2-1)$ we just need to use \ref{eq:A33_sol} and change variable knowing that $d\eta = d(\cos{\theta}) = - \sin{\theta} d\theta$.
\begin{align*}
A_{33} & = 2 \pi \int_0^\pi d\theta \sin{\theta}\,g(\cos{\theta}) \left(3 \cos^2{\theta} - 1\right)\\
& = 2\pi \int_{-1}^1 d\eta\, g(\eta)\, (3\eta^2-1)
\end{align*}
\end{homeworkProblem}

\begin{homeworkProblem}

\textbf{Aplication to the hidrogen atom}\\
Let us consider now as densities those corresponding to the wave fucntions$\psi_{nlm}$ 
of an electron in an hidrogen atom. Every state is characterized by
three quantum numbers: $n$, $l$, and $m$.
Due to the axial symmetry the states that we will analyze have different $n$ e $l$ but always $m=0$.
The densities are $\rho_{nl0}(\vec{r})=|\psi_{nl0}(\vec{r})|^2=f_{nl}(r)g_l(\eta)$.
This is the probability density, to obtain the charge density we should multiply it by $-e$, with $e>0$.  %Estudamos esta densidade para simplificar....
We are going to consider the following cases:
\begin{center}
%\begin{table}
\begin{tabular}{c|c|l|l}
$n$ & $l$ & $f(r)$  & $g(\eta)$\\
\hline
$2$ & $1$ & $\frac{1}{r^2} \frac{1}{24 a_0} (\frac{r}{a_0})^4 e^{-r/a_0}$ & $\frac{3}{4\pi} \eta^2$\\
$3$ & $1$ & $\frac{1}{r^2}\frac{1}{6^3 a_0} (\frac{2r}{3 a_0})^4 (4-\frac{2 r}{3 a_0})^2 e^{-2r/(3a_0)}$ & $\frac{3}{4\pi} \eta^2$\\
$3$ & $2$ & $\frac{1}{r^2}\frac{1}{5\cdot 6^3 a_0} (\frac{2r}{3 a_0})^6 e^{-2r/(3a_0)}$ & $\frac{5}{16\pi} (3\eta^2-1)^2$
\end{tabular}
%\end{table}
\end{center}
where $a_0$ is the Bohr radius.
%\begin{eqnarray}
%&&|\psi_{100}(r,\theta,\varphi)|^2=\frac{1}{\pi a_0^3} e^{-2r/a_0}
%\end{eqnarray}
\begin{enumerate}
\item[9.1.] Show that the given densities are well normalized.
R ∞ That is, for each
state of the upper table, show numerically that $\int_0^\infty f(r)\, r^2\,dr=1$ e 
$2\pi\int_{-1}^1 g(\eta) d\eta =1$. (Obviously, this implies $\int d^3r \rho=1$.)
Chose a number
of integration points such that the absolute error on the normalizarion
is smaller than $10^{-6}$. Indicate which is the numerical method used to
perform the integration.

[Suggestion: work in adimensional units. For instance definig $\tilde{r}=r/a_0$.]

\item[9.2.] Make a figure of $a_0f(r)r^2$ as function of $r/a_0$ or all the states of the
table (all in the figure). To represent the angular part plot in the $x-z$ plane a
$\left(\sin{\theta}\,g(\cos{\theta}), \cos{\theta}\, g(\cos{\theta})\right)$, with the parameter $\theta\in[0,\pi]$.
%Esta imagem corresponde a uma sec\c{c}\~{a}o vertical da parte angular da densidade.
This
curve is built in such a way that the distance between a point in the
curve and the origin of the axis is proporional to the value of $g$ in the
direction defined by $\theta$. Make a figure of this type to represent the angular
part of the case $l = 1$  and another one for $l = 2$. [Note that this
curves represent the information for the value $0$ of the azimuthal angle
$\varphi$. To obtain a tridimensional representation the curves should be rotated
around the axis $z$. ]

\item[9.3.] For each state of the table, compute numerically $F$ and $A_{33}$, using the equations (\ref{eq:FA}) e (\ref{eq:A33}), 
e $Q_{33}=F\, A_{33}$. Use the same numerical method and
the same number of integration points of question (9.1). Considering what
has been seen in the previous points coment the results obtained
\end{enumerate}

\textbf{Solution}
\begin{enumerate}
\item[9.1.] For the radial part we selected the Gauss-Laguerre with weight $x^0 \exp(-x)$, this way the integral is directly from 0 to infinity and we made the change of variable $\frac{r}{a_0} = \tilde{r}$. For the the angular part it was used the Gauss-Legandre method with no change of variable. For all the integrals 100 points were used since the algorithm is pretty fast.
\begin{center}
ì
\begin{tabular}{c|c|l|l}

n & l & $1 - \int f(r)\, r^2\,dr$ &  $1 - 2\pi\int g(\eta) d\eta$\\
\hline
2 & 1 & 2.88850632302e-09 & 3.13082892944e-14\\
3 & 1 & 1.04352250174e-08 & 3.13082892944e-14\\
3 & 2 & 1.12260294394e-08 & 5.09592368303e-14\\
\end{tabular}
\end{center}
\item[9.2.] We can see $a_0f(r)r^2$ as function of $r/a_0$ in figure \ref{radial} .
\begin{figure}[H]
\centering
\includegraphics[width=.8\textwidth]{radial.png}
\caption{Radial rapresentation of the probability density function}
\label{radial}
\end{figure}
And figures \ref{angular} and \ref{second} the two plots for the angular term.
\begin{figure}[H]
\centering
\includegraphics[width=.4\textwidth]{first_angular.png}
\caption{Angular rappresentation of the probability density function for $l = 1$}
\label{angular}
\end{figure}
\begin{figure}[H]
\centering
\includegraphics[width=.4\textwidth]{second_angular.png}
\caption{Angular rapresentation of the probability density function for $l = 2$}
\label{second}
\end{figure}

Since both the plots in \ref{angular} and \ref{second} are prolate we expect $Q_{33} > 0$ so $A_{33} > 0$.

\item[9.3.] Since $F$ is not dimensionless we decided to divide the integral by $(a_0^2)$ to make it so, the same was done for $A_{33} F$
\begin{center}
\begin{tabular}{c|c|l|l|l}

n & l & $F / (a_0^2)$ &  $A_{33}$ &$(A_{33} F) / (a_0^2)$\\
\hline
2 & 1 & 29.999999771 & 0.8 &  23.9999998168\\
3 & 1 & 179.999998377 & 0.8 & 143.999998702\\
3 & 2 & 125.999998633 & 0.571428571428 & 71.9999992187\\
\end{tabular}
\end{center}
As it was said before $A_{33}$ is indeed bigger than 0.
\end{enumerate}
\end{homeworkProblem}
\underline{\large{\textbf{The code used}}}\\


\lstinputlisting[language=Python]{code/solve_zero.py}
\lstinputlisting[language=Python]{code/interpol.py}
\lstinputlisting[language=Python]{code/math_functions.py}
\lstinputlisting[language=Python]{code/Lorenzo_Biasi_code_1.py}


\end{document}

