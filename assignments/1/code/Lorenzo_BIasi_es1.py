from numpy import zeros


# The algorithm for aquiring the newton coeff
def Newtoncoefficient(fi, xi):
    n = len(fi)
    coeff = initializecoefficient(fi, n)
    for j in range(1, n):
        for i in range(0, n - j):
            coeff[i][j] = (coeff[i + 1, j - 1] - coeff[i, j - 1]
                           ) / (xi[i + j] - xi[i])
    return coeff


def initializecoefficient(fi, n):
    coeff = zeros([n, n])
    for i in range(n):
        for j in range(n):
            if j == 0:
                coeff[i][j] = fi[i]
    return coeff


# The result of the interpolation in x. ATTENTION it returns the value of the
# interpolation in x and not a symbolic function
def poli_Newton(fi, xi, x):
    coeff = Newtoncoefficient(fi, xi)
    poli = coeff[0, 0]
    product = 1
    for i in range(1, len(xi)):
        product = product * (x - xi[i - 1])
        poli += coeff[0][i] * product
    return poli
