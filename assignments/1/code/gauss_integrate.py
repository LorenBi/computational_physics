# Takes the point needed from a file of your choice
# and it prepares than to be used in the gauss integral
# input 'Point' 'Weight'
def read_get_gauss(file_name):
    file = open(file_name)
    lst = []
    for line in file:
        lst.append(tuple(float(string) for string in line.split()))
    return lst


# It calculates the integral of f_gauss with the gauss method
def gauss_integral(f_gauss, point_weight):
    integral = 0
    for point, weight in point_weight:
        integral += f_gauss(point) * weight
    return integral