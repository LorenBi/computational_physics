import numpy as np
from scipy.linalg import norm
from copy import deepcopy


# By A we mean a numpy 2darray that we want to use to compute the determinant
# or solve with b an nx1 numpy array. solve_linear will solve Ax = b
# The variable pivot is used to add the partial pivot method in the functions


# Linear solver, determminant, and gauss elemination

# It creates the hilbert matrix
def solve_linear(A, b, pivot=False):
    return backward_subs(*gauss_elimination(A, b))


def det(A, pivot=False):
    # I use the gauss elimination adding an empty array
    U, c = gauss_elimination(A, np.zeros((np.shape(A)[0], 1)), pivot=pivot)
    return np.prod(U.diagonal())


def gauss_elimination(A, b, pivot=False):
    A = np.hstack((A, b))
    for j in range(np.shape(A)[0] - 1):
        partial_pivot(A, j) if pivot else None
        # Here I remove to the the rows from j+1 the row j multiplied by the
        # elements on j column divided by the A[j,j]. Besically this is the
        # algorithm that needs to be repeted n - 1 times
        A[j + 1:][:] = A[j + 1:][:] - np.tile(A[j: j + 1], np.shape(
            A[j + 1:, 0][:, None])) * ((A[j + 1:, j][:, None] / A[j, j]))
    return A[:, 0:-1], A[:, -1:]


def partial_pivot(A, index_pivot):
    next_pivot = A[index_pivot:, index_pivot].argmax()
    if next_pivot != 0:
        print 0
        # here I use advance slicing to swap the lines
        A[[index_pivot, index_pivot + next_pivot],
            :] = A[[index_pivot + next_pivot, index_pivot], :]


def backward_subs(A, c):
    x = np.empty(len(c))
    x[-1] = c[-1] / A[-1][-1]
    for k in range(len(c) - 2, -1, -1):
        x[k] = (c[k] - np.sum(A[k, k + 1:] * x[k + 1:])) / A[k][k]
    return x.reshape((len(x), 1))


# Function for finding eignvectors and eigenvalues

# Some numerical functions
def off(A):
    return norm(A - np.diag(A) * np.identity(len(A)))


def Cosine(chi):
    return np.sqrt((1. + chi) / 2.)


def Sine(chi, tau):
    return np.sign(tau) * np.sqrt((1. - chi) / 2.)


# Returns the sine and cosine needed for the trasfomation
def get_cos_sin(A, index):
    j, k = index
    if A[k, k] == A[j, j]:
        return np.sqrt(2) / 2., np.sqrt(2) / 2
    else:
        tau = (2 * A[j, k]) / (A[k, k] - A[j, j])
        chi = 1 / np.sqrt(1 + tau ** 2)
        return Cosine(chi), Sine(chi, tau)


# It takes a squared matrix A (from numpy.matrix) and it diagonalizes it with
# the Jacobi method. It cycles untill off(A) <= epsilon. The variable 'cycle'
# is to be set to true if you want to use the cycle jacobi method, false if
# you prefer the classical one.
def Jacobi_method(A, epsilon, cycle=True):
    R = np.identity(len(A))
    indexies = get_cycled_indexies(len(A))
    while off(A) > epsilon:
        index = find_next_index(A, indexies, cycle)
        C, S = get_cos_sin(A, index)
        make_diag_with_rotation(A, index, C, S)
        adjourn_rotation(R, index, C, S)
    return A, R


# It accounts for the possibility of the 'classical' Jacobi method or the
# one where you cycle in the upper triagle (0, 1) (0, 2)... (n - 1, n)
def find_next_index(A, indexies, cycle):
    if cycle:
        index = indexies.pop(0)
        indexies.append(index)
    else:
        index = find_max_between(A, indexies)
    return index


# Just returns the indexies on which to cycle on for a nxn matrix
def get_cycled_indexies(n):
    indexies = []
    for i in range(n):
        for j in range(i + 1, n):
            indexies.append((i, j))
    return indexies


# Finds the index of the maximum element of a subsection of the matrix, given
# by the indexies
def find_max_between(A, indexies):
    max_value = - np.inf
    index = None
    for j, k in indexies:
        if abs(A[j, k]) > max_value:
            index = (j, k)
            max_value = abs(A[j, k])
    return index


# It rotates the matrix for putting a_jk and a_kj to 0
def make_diag_with_rotation(A, index, C, S):
    j, k = index
    a_jj = A[j, j]
    a_kk = A[k, k]
    a_jk = A[j, k]
    A[:, j] = C * A[:, j] - S * A[:, k]
    A[:, k] = S * A[j, :].reshape(np.shape(A[:, k])) + \
        C * A[k, :].reshape(np.shape(A[:, k]))
    A[j, :] = A[:, j].reshape(np.shape(A[j, :]))
    A[k, :] = A[:, k].reshape(np.shape(A[k, :]))
    A[j, j] = C * C * a_jj + S * S * a_kk - 2 * S * C * a_jk
    A[j, k] = 0
    A[k, j] = 0
    A[k, k] = S * S * a_jj + C * C * a_kk + 2 * S * C * a_jk


# It adjourns the rotation matrix that at the end will have the eigenvectors
# as columns
def adjourn_rotation(R, index, C, S):
    j, k = index  # (j,k)
    R_rj = deepcopy(R[:, j])
    R[:, j] = C * R_rj - S * R[:, k]
    R[:, k] = S * R_rj + C * R[:, k]
