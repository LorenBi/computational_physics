import numpy as np


# This methods solve differential equation
# interval is a tuple with length 2 and it rapresents the ends of the
# indipendent variable (in our case t)
# N is the number of sub intervals used
# diff_fun is the differnetial function used and defined in the same method
# as the previous
# the initial_cond is a tuple that contains the initial conditions for all the
# variables, even the independent one.
def Rounge_Kutta_4(interval, N, diff_fun, initial_cond, dtype=float):
    h = abs(interval[1] - interval[0]) / float(N)
    t = np.zeros(N)
    x = np.zeros((len(initial_cond) - 1, N), dtype=dtype)
    initialize(t, x, initial_cond)
    for n in range(N - 1):
        # No need of running a cycle on the number of variables since we use
        # an ndarray format.
        k_1 = diff_fun(t[n], x[:, n])
        k_2 = diff_fun(t[n] + h / 2, x[:, n] + (h * k_1) / 2)
        k_3 = diff_fun(t[n] + h / 2, x[:, n] + (h * k_2) / 2)
        k_4 = diff_fun(t[n] + h, x[:, n] + h * k_3)
        x[:, n + 1] = x[:, n] + (h * (k_1 + 2 * k_2 + 2 * k_3 + k_4)) / 6
        t[n + 1] = t[n] + h
    return t, x


# It gives the initial conditions
def initialize(t, x, initial_cond):
    t[0] = initial_cond[0]
    for i in range(1, len(initial_cond)):
        x[i - 1][0] = initial_cond[i]
