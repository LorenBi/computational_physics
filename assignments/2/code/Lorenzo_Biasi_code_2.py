from linear_equations import solve_linear, Jacobi_method
from differential_solver import Rounge_Kutta_4
import matplotlib.pyplot as plt
import numpy as np

'''
This is the file containing all the solution to the problems of the assignment.
It is divided in parts to make it easier to use. It will save the figures in
a folder.
'''


def part11():
    # We solve the matrix system_A that rapresents a circuit
    system_A = np.array([[3, 0, -1, -1, 0, 0, 0],
                         [0, 3, 0, -1, -1, 0, 0],
                         [2, 0, -3, 0, 0, 1, 0],
                         [2, 1, 0, -6, 0, 1, 2],
                         [0, 1, 0, 0, -3, 0, 2],
                         [0, 0, 1, 1, 0, -3, 0],
                         [0, 0, 0, 1, 1, 0, -3]], dtype=float)
    # system_b is the other part of the linear system of equations
    system_b = np.array([10, 10, 0, 0, 0, 0, 0], dtype=float)
    potentials = solve_linear(system_A, system_b.reshape((7, 1)), pivot=True)
    print 'Part I, problem 1 \n'
    print 'The solution for the potentials is:\n', potentials
    print '\nWe can see that the solution satisfies the matrix:'
    print np.dot(system_A, potentials)


def part12():
    # We define 2 functions for each system proposed
    def armonic_cicruit(t, x):
        L = 1
        C = 0.05
        dQ = x[1]
        dI = - x[0] / (L * C)
        return np.array([dQ, dI])

    def forced_circuit(t, x):
        L = 1
        C = 0.05
        V_a = 0.1
        omega = 5
        R = 0.1
        dQ = x[1]
        dI = - x[0] / (L * C) - x[1] * R + (V_a / L) * np.cos(omega * t)
        return np.array([dQ, dI])

    # In this part we simply solve the differential equation with Rounge Kutta
    # and then we plot what we need
    t, x = Rounge_Kutta_4((0, 125), 10000, armonic_cicruit, (0, 0, 1))
    fig = plt.figure('armonic')
    plt.plot(t, x[0, :])
    plt.xlabel('t [s]')
    plt.ylabel('Q(t) [C]')
    plt.xlim([0, 10])
    plt.grid(True)
    fig.savefig('armonic.png')

    t, x = Rounge_Kutta_4((0, 125), 10000, forced_circuit, (0, 0, 1))
    fig = plt.figure('forced_Q')
    plt.plot(t, x[0, :])
    plt.xlabel('t [s]')
    plt.ylabel('Q(t) [C]')
    plt.grid(True)
    fig.savefig('forced_Q.png')

    fig = plt.figure('forced_I')
    plt.plot(t, x[1, :])
    plt.xlabel('t [s]')
    plt.ylabel('I(t) [A]')
    plt.grid(True)
    plt.ylim([-1, 1])
    fig.savefig('forced_I.png')

    fig = plt.figure('forced_QI')
    plt.plot(x[0, :], x[1, :])
    plt.xlabel('Q [C]')
    plt.ylabel('I(t) [A]')
    plt.ylim([-1.5, 1.5])
    plt.grid(True)
    plt.savefig('forced_QI.png')


def part21():
    # The system for the 3 masses is defined in molecular_vibration.
    def molecular_vibration(t, x):
        m1 = 1.
        m2 = 16.
        k = 1.
        a1 = - (k / m1) * (x[0] - x[1])
        a2 = - (k / m2) * (x[1] - x[0]) - (k / m2) * (x[1] - x[2])
        a3 = - (k / m1) * (x[2] - x[1])
        v1 = x[3]
        v2 = x[4]
        v3 = x[5]
        return np.array([v1, v2, v3, a1, a2, a3])
    # We loop on all the initial conditions asked by the problem.
    initial_conditions = [(0, 0, 0, 0, 1, 1, 1),
                          (0, 0, 0, 0, 1, 0, 1),
                          (0, 0, 0, 0, 1, 0, -1),
                          (0, 0, -1, -1, 0, 0, 0)]

    for i, initial in enumerate(initial_conditions):
        t, x = Rounge_Kutta_4((0, 200), 10000, molecular_vibration, initial)

        fig = plt.figure(str(i) + 'vibrations')
        plt.plot(t, x[0, :], t, x[1, :], t, x[2, :])
        plt.grid(True)
        plt.xlabel('t [s]')
        plt.ylabel('x(t) [m]')
        legend = plt.legend([r'x_1', r'x_2', r'x_3'], loc=2)
        legend.get_frame().set_facecolor('white')

        fig.savefig(str(i) + '_vibrations.png')


def part22():
    # Oscilatting_nxn stands for the matrix decribing a system with n masses
    oscillating_3x3 = np.matrix([[1, -1, 0],
                                 [-1, 2, -1],
                                 [0, -1, 1]], dtype=float)
    # eign_val_3 is a matrix with on the diagonal the eigen values, Be careful
    eign_val_3, eign_vects_3 = Jacobi_method(oscillating_3x3, 10 ** -7)
    print 'Part II, problem 2 \n'
    print 'The columns contain each eigen vector'
    print eign_vects_3
    print 'For the relative eigen value'
    print eign_val_3.diagonal()

    # 9 masses system
    oscillating_9x9 = np.matrix([[1, -1, 0, 0, 0, 0, 0, 0, 0],
                                 [-1, 2, -1, 0, 0, 0, 0, 0, 0],
                                 [0, -1, 2, -1, 0, 0, 0, 0, 0],
                                 [0, 0, -1, 2, -1, 0, 0, 0, 0],
                                 [0, 0, 0, -1, 2, -1, 0, 0, 0],
                                 [0, 0, 0, 0, -1, 2, -1, 0, 0],
                                 [0, 0, 0, 0, 0, -1, 2, -1, 0],
                                 [0, 0, 0, 0, 0, 0, -1, 2, -1],
                                 [0, 0, 0, 0, 0, 0, 0, -1, 1]], dtype=float)

    eign_val_9, eign_vects_9 = Jacobi_method(oscillating_9x9, 10 ** -5)
    fig, ax = plt.subplots()

    # Plotting the 4 normal modes with lower frequency
    for i, mark, n in zip(range(4), ['*--', 'o--', '^--', 's--'], range(1, 5)):
        index = eign_val_9.diagonal().argsort()[0, :4][0, i]
        ax.plot(np.arange(1, 10), eign_vects_9[
                :, index] / eign_vects_9[0, index], mark, label=r'$\omega_' + str(n) + '$')
    print eign_val_9.diagonal()
    ax.set_xlabel('i')
    ax.set_ylabel(r'$X_i$')
    ax.legend()
    fig.savefig('normal_modes.png')


if __name__ == '__main__':
    part11()
    print '\n'
    part12()
    print '\n'
    part21()
    print '\n'
    part22()
    print '\nThe outputs of some function are images'
    # plt.show()
