\documentclass{article}

\usepackage{fancyhdr}
\usepackage{extramarks}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{tikz}
\usepackage[plain]{algorithm}
\usepackage{algpseudocode}
\usepackage[T1]{fontenc}
\usepackage{listings}

\DeclareFixedFont{\ttb}{T1}{txtt}{bx}{n}{9} % for bold
\DeclareFixedFont{\ttm}{T1}{txtt}{m}{n}{9}  % for normal
% Defining colors
\usepackage{color}
\definecolor{deepblue}{rgb}{0,0,0.5}
\definecolor{deepred}{rgb}{0.6,0,0}
\definecolor{deepgreen}{rgb}{0,0.5,0}


\usepackage{listings}

% Python style for highlighting
\lstset{
language=Python,
backgroundcolor=\color{white},
basicstyle=\ttm,
otherkeywords={self},            
keywordstyle=\ttb\color{deepblue},
emph={MyClass,__init__},          
emphstyle=\ttb\color{deepred},    
stringstyle=\color{deepgreen},
commentstyle=\color{red},
frame=tb,                         
showstringspaces=false            
}




\usetikzlibrary{automata,positioning}

%
% Basic Document Settings
%

\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\linespread{1.1}

\pagestyle{fancy}
\lhead{\hmwkAuthorName}
\chead{\hmwkClass: \hmwkTitle}
\rhead{\firstxmark}
\lfoot{\lastxmark}
\cfoot{\thepage}

\renewcommand\headrulewidth{0.4pt}
\renewcommand\footrulewidth{0.4pt}

\setlength\parindent{0pt}

%
% Create Problem Sections
%

\newcommand{\enterProblemHeader}[1]{
    \nobreak\extramarks{}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
}

\newcommand{\exitProblemHeader}[1]{
    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
    \stepcounter{#1}
    \nobreak\extramarks{Problem \arabic{#1}}{}\nobreak{}
}

\setcounter{secnumdepth}{0}
\newcounter{partCounter}
\newcounter{homeworkProblemCounter}
\setcounter{homeworkProblemCounter}{1}
\nobreak\extramarks{Problem \arabic{homeworkProblemCounter}}{}\nobreak{}

%
% Homework Problem Environment
%
% This environment takes an optional argument. When given, it will adjust the
% problem counter. This is useful for when the problems given for your
% assignment aren't sequential. See the last 3 problems of this template for an
% example.
%
\newenvironment{homeworkProblem}[1][-1]{
    \ifnum#1>0
        \setcounter{homeworkProblemCounter}{#1}
    \fi
    \section{Problem \arabic{homeworkProblemCounter}}
    \setcounter{partCounter}{1}
    \enterProblemHeader{homeworkProblemCounter}
}{
    \exitProblemHeader{homeworkProblemCounter}
}

%
% Homework Details
%   - Title
%   - Due date
%   - Class
%   - Section/Time
%   - Instructor
%   - Author
%

\newcommand{\hmwkTitle}{Homework\ \#2}
\newcommand{\hmwkDueDate}{April 11, 2017}
\newcommand{\hmwkClass}{Computational Physics}
\newcommand{\hmwkClassInstructor}{Professor Isaac Vidana}
\newcommand{\hmwkAuthorName}{\textbf{Lorenzo Biasi}}

%
% Title Page
%

\title{
    \vspace{2in}
    \textmd{\textbf{\hmwkClass:\ \hmwkTitle}}\\
    \normalsize\vspace{0.1in}\small{Due\ on\ \hmwkDueDate\ at 23.00}\\
    \vspace{0.1in}\large{\textit{\hmwkClassInstructor}}
    \vspace{3in}
}

\author{\hmwkAuthorName}
\date{}

\renewcommand{\part}[1]{\textbf{\large Part \Alph{partCounter}}\stepcounter{partCounter}\\}

%
% Various Helper Commands
%

% Useful for algorithms
\newcommand{\alg}[1]{\textsc{\bfseries \footnotesize #1}}

% For derivatives
\newcommand{\deriv}[1]{\frac{\mathrm{d}}{\mathrm{d}x} (#1)}

% For partial derivatives
\newcommand{\pderiv}[2]{\frac{\partial}{\partial #1} (#2)}

% Integral dx
\newcommand{\dx}{\mathrm{d}x}

% Alias for the Solution section header
\newcommand{\solution}{\textbf{\large Solution}}

% Probability commands: Expectation, Variance, Covariance, Bias
\newcommand{\E}{\mathrm{E}}
\def\permille{\ensuremath{{}^\text{o}\mkern-5mu/\mkern-3mu_\text{oo}}}


\begin{document}

\maketitle

\pagebreak
\underline{\large{\textbf{Part I: Electrical circuits}}}\\

\begin{homeworkProblem}
\textbf{Solution} \\
Using the Kirchoff law and Ohm law we can write down a system of equations:
\begin{equation*}
    \begin{cases} 
      \frac{V_h}{R_{12}} = \frac{V_e - V_h}{R_9} + \frac{V_f - V_h}{R_{10}}\\ 
      \frac{V_g}{R_{11}} = \frac{V_e - V_g}{R_8} + \frac{V_d - V_g}{R_7}\\
      \frac{V_f - V_h}{R_{10}} =  \frac{V_c - V_f}{R_6}\\
      \frac{V_e - V_g}{R_8} + \frac{V_e - V_h}{R_9} = \frac{V_b - V_e}{R_4} + \frac{V_c - V_e}{R_5}\\
      \frac{V_d - V_g}{R_7} = \frac{V_b - V_d}{R_3}\\
      \frac{V_c - V_f}{R_6} + \frac{V_c - V_e}{R_5} = \frac{V_a - V_c}{R_2}\\
      \frac{V_b - V_d}{R_3} + \frac{V_b - V_e}{R_4} = \frac{V_a - V_b}{R_1}\\
    \end{cases}
\end{equation*}
If we write down the values for the resistors and remember that $V_a = 10$, we get a system of equation that can be written as the matrix given.
\end{homeworkProblem}


\begin{homeworkProblem}
\textbf{Solution} \\
By solving the equations using the gauss elimination and backward substitution we find the value in Eq. \ref{eq:potentials}
\begin{equation} \label{eq:potentials}
 \begin{pmatrix}
  V_b\\
  V_c\\
  V_d\\
  V_e\\
  V_f\\
  V_g\\
  V_h\\

 \end{pmatrix}
  = 
 \begin{pmatrix}
  6.9444\\
  6.3889\\
  5.8333\\
  5.0       \\
  4.1667\\
  3.6111\\
  3.0556\\
 \end{pmatrix} V
\end{equation}
We than can multiply the given matrix with the result we got an see that exept for a small approximation error the result satisfies the system.
\begin{equation}
 \begin{pmatrix}
 10\\
 10\\
 0\\
 1.776 \times 10^{-15}\\
 -1.776 \times 10^{-15}\\
 0\\
 0\\

 \end{pmatrix}
\end{equation}
\end{homeworkProblem}


\begin{homeworkProblem}
\textbf{Solution} \\
By substituting $I(t) = dQ(t) / dt$ and dividing the given equation by $L$ we get the Eq. \ref{eq:general_circuit}
\begin{equation}\label{eq:general_circuit}
  \ddot{Q} + \frac{R}{L} \dot{Q} + \frac{1}{LC} Q =\frac{1}{L} V(t)
\end{equation}
Equation \ref{eq:general_circuit} is analogous to the one of a forced dumped armonic oscillator (Eq. \ref{eq:dumped_oscill}) where $\frac{R}{L} = 2 \lambda$, $ \frac{1}{LC} = \omega^2$ and $\frac{1}{L} = \frac{1}{m}$
\begin{equation}\label{eq:dumped_oscill}
  \ddot{x} + 2\lambda \dot{x} + \omega^2 x =\frac{1}{m} F(t)
\end{equation}
\end{homeworkProblem}


\begin{homeworkProblem}
We can solve the equation with no forcing potential and $R$= 0 finding the characteristic equation for \ref{eq:armonic_simple}
\begin{equation}\label{eq:armonic_simple}
  \ddot{Q} + \omega^2 Q = 0
\end{equation}
where $\omega^2 = \frac{1}{LC}$. The characteristic equation is simply 
\begin{equation}
  \lambda^2 + \omega^2 = 0
\end{equation}
its roots are $\lambda_1 = i \omega$ and $\lambda_2 = -i \omega$. This way we find that the solution has the  form $Q(t) = C_{I} e^{i\omega} + C_{II} e^{-i\omega}$. If we impose that $Q(0) = 0$C and $\frac{d Q}{dt} = 1$ A we find that our solution is $Q(t) = \frac{1\text{A}}{\omega} \sin{\omega t}$.
We can see the result in Fig. \ref{fig:armonic} is compatible with what we found analytically. In fact the amplitude is $1 \text{A}\sqrt{LC} = 0.225$C and the period is $2 \pi \sqrt{LC} = 1.405$s The method used was the Rounge Kutta 4 with a number of subintervals of $10^4$ over 125 s.
\begin{figure}[h!]
  \begin{center}
    \includegraphics[width=7.0cm]{code/armonic.png}
  \end{center}
  \caption{Armonic oscillating circuit}
  \label{fig:armonic}
\end{figure}


\end{homeworkProblem}


\begin{homeworkProblem}
We can see in Fig \ref{fig:forced_I} and \ref{fig:forced_Q} dumpens with and exponential behaviour making the transient solution less and less relevant and settles to the steady-state solution thanks to the effect of the forcing voltage. In Fig. \ref{fig:forced_QI} we can see how the system moves closer and closer to the center, but does not converge in it thanks to the forcing voltage. This can be seen if think about the total energy $E$ in the system like in equation \ref{eq:tot_energy}.
\begin{equation}\label{eq:tot_energy}
  E = \frac{1}{2} L I^2 + \frac{1}{2} \frac{Q^2}{C} \implies 1 = \frac{I^2}{\frac{2 E}{L}} + \frac{Q^2}{2CE}
\end{equation}
We can recognize the equation of an ellipse. We can now understand why with the energy that diminishes in the system the axes shorten.\\
\begin{figure}[h!]
  \begin{center}
    \includegraphics[width=7.0cm]{code/forced_I.png}
  \end{center}
  \caption{The current in function of time in a dumped force system}
  \label{fig:forced_I}
\end{figure}

\begin{figure}[h!]
  \begin{center}
    \includegraphics[width=7.0cm]{code/forced_Q.png}
  \end{center}
  \caption{The charge in function of time in a dumped force system}
  \label{fig:forced_Q}
\end{figure}

\begin{figure}[h!]
  \begin{center}
    \includegraphics[width=7.0cm]{code/forced_QI.png}
  \end{center}
  \caption{The phase space in a dumped force system}
  \label{fig:forced_QI}
\end{figure}
\end{homeworkProblem}
\pagebreak

\underline{\large{\textbf{Part II: Vibration of a linear molecule}}}\\
\begin{homeworkProblem}
We can write the equation that describes the system by using Newton's second law $\vec{F} = m {a}$ and the Hooke's law. We in fact that the force on the first mass will linear with the compression of the spring and in the negative direction when the spring is compressed, so $F_1 = - k_1(x_1 - x_2) = m_1 \ddot(x_1)$. For the second mass there will be two forces, $F_{21}$ and $F_{22}$. Also $F_{21}$ will be linear with the compression of the spring but in the opposite direction and $F_{21}$ will be proportional to $x_2 - x_3$ and negative when the spring is compressed, so $F_{12} + F_{22} = - k_1(x_2 - x_1) - k_2(x_2 - x_3)$. The calculations for the third spring are analogous to the first one.
\end{homeworkProblem}

\begin{homeworkProblem}
In Fig. \ref{fig:0_vibrations} we have the system with initial conditions 
\begin{equation*}
(x_1, \dot{x_1})_{t=0} = (0, 1),~ (x_2, \dot{x_2})_{t=0} = (0, 1), ~ (x_3, \dot{x_3})_{t=0} = (0, 1)
\end{equation*}
We chose to use a number of subintervals of $10^4$ on 200 s, so we worked with an h = 0.02s
It might seem strange that the system is not oscillating but since all the masses have the same velocity and the springs start in the equilibrium position, then all the system moves in one diraction with given velocity without ever oscillating.\\
In Fig. \ref{fig:1_vibrations} we have the system with initial conditions 
\begin{equation*}
(x_1, \dot{x_1})_{t=0} = (0, 1),~ (x_2, \dot{x_2})_{t=0} = (0, 0), ~ (x_3, \dot{x_3})_{t=0} = (0, 1)
\end{equation*}
In this case the system looks similar to the previous system, with the added factor of the oscillation. This is due to the fact that the total momentum of the system is still not zero and the middle mass starts at rest, this causes the springs to contract or distend initiating the oscillation. We can also notice that the system is much slower of the first one, this is because the central mass is the one with the greatest mass, so in the first system is the fist mass that gives the biggest contribution to the momentum.\\
In Fig. \ref{fig:2_vibrations} we have the system with initial conditions 
\begin{equation*}
(x_1, \dot{x_1})_{t=0} = (0, 1),~ (x_2, \dot{x_2})_{t=0} = (0, 0), ~ (x_3, \dot{x_3})_{t=0} = (0, -1)
\end{equation*}
In this case the total momentum of the system is null, so it does not move. We can see that the second mass does not move, this is because the other two masses are moving simulataneously in opposite directions and since they weight the same, the central object does not move.\\
In Fig. \ref{fig:3_vibrations} we have the system with initial conditions 
\begin{equation*}
(x_1, \dot{x_1})_{t=0} = (0, 0),~ (x_2, \dot{x_2})_{t=0} = (-1, 0), ~ (x_3, \dot{x_3})_{t=0} = (-1, 0)
\end{equation*}
Also in this case the system has total momentum null. In this case the spring that initiate the oscillation is the first, which is the only one contracted.

\begin{figure}[h!]
  \begin{center}
    \includegraphics[width=8.0cm]{code/0_vibrations.png}
  \end{center}
  \caption{Vibrations with the initial conditions}
  \label{fig:0_vibrations}
\end{figure}

\begin{figure}[h!]
  \begin{center}
    \includegraphics[width=8.0cm]{code/1_vibrations.png}
  \end{center}
  \caption{Vibrations with the initial conditions}
  \label{fig:1_vibrations}
\end{figure}

\begin{figure}[h!]
  \begin{center}
    \includegraphics[width=8.0cm]{code/2_vibrations.png}
  \end{center}
  \caption{Vibrations with the initial conditions}
  \label{fig:2_vibrations}
\end{figure}

\begin{figure}[h!]
  \begin{center}
    \includegraphics[width=8.0cm]{code/3_vibrations.png}
  \end{center}
  \caption{Vibrations with the initial conditions}
  \label{fig:3_vibrations}
\end{figure}
\end{homeworkProblem}
\pagebreak
\begin{homeworkProblem}
Using the equation given in the problem 6, we can sustitute all $x_i$ with $X_i e^{i\omega t}$. By doing that we obtain:
\begin{equation*}
    \begin{cases} 
    - m \omega^2 X_1 e^{i\omega t} = - k (X_1 - X_2) e^{i\omega t} \\
    - m \omega^2 X_2 e^{i\omega t} = - k (X_2 - X_1) e^{i\omega t} - k (X_2 - X_3) e^{i\omega t} \\
    - m \omega^3 X_2 e^{i\omega t} = - k (X_3 - X_2) e^{i\omega t} \\
    \end{cases}
\end{equation*}
By reelaborating the equation we get:
\begin{equation*}
    \begin{cases} 
    \frac{\omega^2}{\omega_0^2}X_1 = (X_1 - X_2) \\
    \frac{\omega^2}{\omega_0^2}X_2 = (- X_1 + 2 X_2 - X_3)\\
    \frac{\omega^2}{\omega_0^2}X_3 = (X_3 - X_2) \\
    \end{cases}
\end{equation*}
Which is analogous to the matrix given by the problem.
\end{homeworkProblem}

\begin{homeworkProblem}
The result obtained by using the Jacobi method are:
\begin{equation*}
v_1 =
	 \begin{pmatrix}
	 	1 \\
	 	1 \\
	 	1 \\
	 \end{pmatrix}
\, \lambda_1 = -10^{-16};
 \quad 
 v_2 =
	 \begin{pmatrix}
	 	1 \\
	 	-2 \\
	 	1 \\
	 \end{pmatrix}
\, \lambda_2 = 3;
 \quad 
 v_3 =
	 \begin{pmatrix}
	 	1 \\
	 	0 \\
	 	-1 \\
	 \end{pmatrix}
\, \lambda_3 = 1
\end{equation*}
First of all we can see that the vectors are all linearly independent as we would expect. Secondly we know that the the component of each vector rappresent the relative dislocation from its equilibrium position compared to others. So we can easily understand that the first eignvector is the one where all the masses have the same relative dislocation, in fact the frequency is zero. for the other two we can identify the case in which the center mass (the heaviest) has double relative distance than the other two masses and the case where the center mass is not moving and the other two masses move in opposite directions.
\end{homeworkProblem}

\begin{homeworkProblem}
If we generalize the equation for 9 masses we get a similar matrix to the previous onw:
\begin{equation*}
	\begin{pmatrix}
	1 & -1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
	-1 & 2 & -1 & 0 & 0 & 0 & 0 & 0 & 0 \\
	0 & -1 & 2 & -1 & 0 & 0 & 0 & 0 & 0 \\
	0 & 0 & -1 & 2 & -1 & 0 & 0 & 0 & 0 \\
	0 & 0 & 0 & -1 & 2 & -1 & 0 & 0 & 0 \\
	0 & 0 & 0 & 0 & -1 & 2 & -1 & 0 & 0 \\
	0 & 0 & 0 & 0 & 0 & -1 & 2 & -1 & 0 \\
	0 & 0 & 0 & 0 & 0 & 0 & -1 & 2 & -1 \\
	0 & 0 & 0 & 0 & 0 & 0 & 0 & -1 & 1 \\
	\end{pmatrix}
\end{equation*}
Solving this we are able to plot the four normal modes with smaller frequency\footnote{We decided to normalize the modes to have the first component equal to 1. This allows for better comparison.}:

\begin{figure}[h!]
  \begin{center}
    \includegraphics[width=10.0cm]{code/normal_modes.png}
  \end{center}
  \caption{The four normal modes with lower frequency. The x-axis what mass we are focusing on and on the y-axis the value of $X_i$}
  \label{fig:1_vibrations}
\end{figure}
\end{homeworkProblem}
\pagebreak

\lstinputlisting[language=Python]{code/linear_equations.py}
\lstinputlisting[language=Python]{code/differential_solver.py}
\lstinputlisting[language=Python]{code/Lorenzo_Biasi_code_2.py}

\end{document}
